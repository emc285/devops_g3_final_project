package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US008CreatePersonTransactionService;
import switch2019.project.dtoLayer.dtos.CreatePersonTransactionDTO;
import switch2019.project.dtoLayer.dtos.PersonDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreatePersonTransactionDTOAssembler;

/**
 * The type Us 008 create person transaction controller.
 *
 * @author Francisco Gaspar
 */
@Controller
public class US008CreatePersonTransactionController {

    //US08 - Como utilizador, quero criar
    // um movimento, atribuindo-lhe
    // um valor, a data (atual, por omissão), uma descrição, uma categoria, uma conta de crédito e outra de débito.

    //public boolean createTransactionAsMember(Person person, Category category, String type, String description, double amount, LocalDate date, Account debitAccount, Account creditAccount) {

    @Autowired
    private US008CreatePersonTransactionService us008CreatePersonTransactionService;

    /**
     * Instantiates a new Us 008 create person transaction controller.
     *
     * @param us008CreatePersonTransactionService the us 008 create person transaction service
     */
    public US008CreatePersonTransactionController(US008CreatePersonTransactionService us008CreatePersonTransactionService) {
        this.us008CreatePersonTransactionService = us008CreatePersonTransactionService;
    }

    /**
     * Create transaction as person boolean dto.
     *
     * @param email                   the email
     * @param denominationCategory    the denomination category
     * @param type                    the type
     * @param description             the description
     * @param amount                  the amount
     * @param denominationAccountDeb  the denomination account deb
     * @param denominationAccountCred the denomination account cred
     * @return the boolean dto
     */
    public PersonDTO createTransactionAsPerson(String email, String denominationCategory, String type, String description, double amount, String denominationAccountDeb, String denominationAccountCred, String date) {
        CreatePersonTransactionDTO createPersonTransactionDTO = CreatePersonTransactionDTOAssembler.createDTOFromPrimitiveTypes(email, denominationCategory, type, description, amount, denominationAccountDeb, denominationAccountCred, date);
        return us008CreatePersonTransactionService.createTransactionAsPerson(createPersonTransactionDTO);
    }
}
