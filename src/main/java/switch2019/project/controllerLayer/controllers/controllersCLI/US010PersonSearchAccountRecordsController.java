package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US010PersonSearchAccountRecordsService;
import switch2019.project.dtoLayer.dtos.PersonSearchAccountRecordsInDTO;
import switch2019.project.dtoLayer.dtos.SearchAccountRecordsOutDTO;
import switch2019.project.dtoLayer.dtosAssemblers.PersonSearchAccountRecordsInDTOAssembler;

/**
 * US010 - As a user, I want to obtain the transactions of a given account, for a given period
 */
@Controller
public class US010PersonSearchAccountRecordsController {

    @Autowired
    private US010PersonSearchAccountRecordsService us010SearchPersonAccountRecordsService;

    /**
     * Instantiates a new controller, for getting a person transactions, for a given account, within a given period.
     *
     * @param us010PersonSearchAccountRecordsService
     */
    public US010PersonSearchAccountRecordsController(US010PersonSearchAccountRecordsService us010PersonSearchAccountRecordsService) {
        this.us010SearchPersonAccountRecordsService = us010PersonSearchAccountRecordsService;
    }

    /**
     * Gets person transactions, for a given account, within a given period
     *
     * @param personEmail         the person email
     * @param accountDenomination the denomination of the account to search
     * @param startDate           the start date of the period to search
     * @param endDate             the end date of the period to search
     * @return the person transactions, for a given account, within a given period
     */
    public SearchAccountRecordsOutDTO getPersonAccountTransactionsWithinPeriod(String personEmail, String accountDenomination, String startDate, String endDate) {
        PersonSearchAccountRecordsInDTO personAccountTransactionsInDTO = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);
        return us010SearchPersonAccountRecordsService.getPersonAccountTransactionsWithinPeriod(personAccountTransactionsInDTO);
    }
}
