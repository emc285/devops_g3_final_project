package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US001CheckIfSiblingsService;
import switch2019.project.dtoLayer.dtos.BooleanDTO;
import switch2019.project.dtoLayer.dtos.CheckIfSiblingsDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CheckIfSiblingsDTOAssembler;

/**
 * The type Us 001 check if siblings controller.
 *
 * @author Ala Matos
 */
//Como gestor de sistema, quero saber se determinada pessoa é irmão/irmã de outra.
// É irmão/irmã de outra se tem a mesma mãe ou o mesmo pai, ou se na lista de irmãos está incluída a outra.
// A relação de irmão é bidirecional, i.e. se A é irmão de B, então B é irmão de A.
@Controller
public class US001CheckIfSiblingsController {
    private US001CheckIfSiblingsService us001CheckIfSiblingsService;

    /**
     * Instantiates a new Us 001 check if siblings controller.
     *
     * @param us001CheckIfSiblingsService the us 001 check if siblings service
     */
    public US001CheckIfSiblingsController(US001CheckIfSiblingsService us001CheckIfSiblingsService) {
        this.us001CheckIfSiblingsService = us001CheckIfSiblingsService;
    }

    /**
     * Check if brother boolean dto.
     *
     * @param email        the email
     * @param siblingEmail the sibling email
     * @return the boolean dto
     */
    public BooleanDTO checkIfBrother(String email, String siblingEmail) {
        CheckIfSiblingsDTO checkIfSiblingsDTO = CheckIfSiblingsDTOAssembler.createDTOFromPrimitiveTypes(email, siblingEmail);

        return us001CheckIfSiblingsService.checkIfSiblings(checkIfSiblingsDTO);

    }

}
