package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switch2019.project.dtoLayer.dtos.CreatePersonTransactionDTO;
import switch2019.project.dtoLayer.dtos.DeletePersonTransactionDTO;
import switch2019.project.dtoLayer.dtos.PersonDTO;
import switch2019.project.dtoLayer.dtos.UpdatePersonTransactionDTO;
import switch2019.project.dtoLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.LedgerID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IAccountRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.ICategoryRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.ILedgerRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.time.LocalDate;
import java.util.Optional;

/**
 * The type Us 008 create person transaction service.
 *
 * @author Francisco Gaspar
 */
@Service
public class US008CreatePersonTransactionService {

    @Autowired
    private IPersonRepository personRepository;
    @Autowired
    private IAccountRepository accountRepository;
    @Autowired
    private ILedgerRepository ledgerRepository;
    @Autowired
    private ICategoryRepository categoryRepository;

    /**
     * The constant SUCCESS.
     */
    //Return messages
    public final static String SUCCESS = "Transaction created and added";
    /**
     * The constant CATEGORY_DOES_NOT_EXIST.
     */
    public final static String CATEGORY_DOES_NOT_EXIST = "Category doesn't exist";
    /**
     * The constant ACCOUNT_DEB_DOES_NOT_EXIST.
     */
    public final static String ACCOUNT_DEB_DOES_NOT_EXIST = "Debit Account doesn't exist";
    /**
     * The constant ACCOUNT_CRED_DOES_NOT_EXIST.
     */
    public final static String ACCOUNT_CRED_DOES_NOT_EXIST = "Credit Account doesn't exist";
    /**
     * The constant PERSON_DOES_NOT_EXIST.
     */
    public final static String PERSON_DOES_NOT_EXIST = "Person doesn't exist";

    /**
     * Instantiates a new Us 008 create person transaction service.
     *
     * @param personRepository   the person repository
     * @param accountRepository  the account repository
     * @param ledgerRepository   the ledger repository
     * @param categoryRepository the category repository
     */
    public US008CreatePersonTransactionService(IPersonRepository personRepository, IAccountRepository accountRepository, ILedgerRepository ledgerRepository, ICategoryRepository categoryRepository) {
        this.personRepository = personRepository;
        this.accountRepository = accountRepository;
        this.ledgerRepository = ledgerRepository;
        this.categoryRepository = categoryRepository;
    }

    /**
     * Create transaction as person boolean dto.
     *
     * @param createPersonTransactionDTO the create person transaction dto
     * @return the boolean dto
     */
    public PersonDTO createTransactionAsPerson(CreatePersonTransactionDTO createPersonTransactionDTO) {
        Person person;

        //Person
        PersonID personID = PersonID.createPersonID(createPersonTransactionDTO.getEmail());
        Optional<Person> optPerson = personRepository.findById(personID);

        //If person does not exist, transaction will not be created
        if (optPerson.isPresent()) {
            person = optPerson.get();

        } else {

            throw new NotFoundArgumentsBusinessException(PERSON_DOES_NOT_EXIST);

        }

        //Category
        CategoryID categoryID = CategoryID.createCategoryID(createPersonTransactionDTO.getDenominationCategory(), personID);
        boolean categoryExistsInRepo = categoryRepository.existsById(categoryID);

        //Debit Account
        AccountID debitAccountID = AccountID.createAccountID(createPersonTransactionDTO.getDenominationAccountDeb(), personID);
        boolean accountToDebitExistsInRepo = accountRepository.existsById(debitAccountID);

        //Credit Account
        AccountID creditAccountID = AccountID.createAccountID(createPersonTransactionDTO.getDenominationAccountCred(), personID);
        boolean accountToCreditExistsInRepo = accountRepository.existsById(creditAccountID);

        //Ledger
        LedgerID ledgerID = person.getLedgerID();
        Optional<Ledger> optLedger = ledgerRepository.findById(ledgerID);

        String type = createPersonTransactionDTO.getType();
        String description = createPersonTransactionDTO.getDescription();
        double amount = createPersonTransactionDTO.getAmount();

        if (!(categoryExistsInRepo)) {

            //"Category does not exist"
            throw new NotFoundArgumentsBusinessException(CATEGORY_DOES_NOT_EXIST);


        } else if (!(accountToDebitExistsInRepo)) {

            //"Debit Account does not exist"
            throw new NotFoundArgumentsBusinessException(ACCOUNT_DEB_DOES_NOT_EXIST);


        } else if (!(accountToCreditExistsInRepo)) {

            //"Credit Account does not exist"
            throw new NotFoundArgumentsBusinessException(ACCOUNT_CRED_DOES_NOT_EXIST);

        } else {

            LocalDate date = LocalDate.parse(createPersonTransactionDTO.getDate());

            Ledger ledger = optLedger.get();
            ledger.createAndAddTransactionWithDate(categoryID, type, description, amount, date, debitAccountID, creditAccountID);
            ledgerRepository.addAndSaveTransaction(ledger);

        }

        return PersonDTOAssembler.createDTOFromDomainObject(person.getPersonID().getEmail(), person.getLedgerID(), person.getName(), person.getBirthdate(), person.getBirthplace(), person.getFather(), person.getMother());
    }

    public PersonDTO updatePersonTransaction(UpdatePersonTransactionDTO updatePersonTransactionDTO){

        Person person;

        //Person
        PersonID personID = PersonID.createPersonID(updatePersonTransactionDTO.getEmail());
        Optional<Person> optPerson = personRepository.findById(personID);

        //If person does not exist, transaction will not be created
        if (optPerson.isPresent()) {
            person = optPerson.get();

        } else {

            throw new NotFoundArgumentsBusinessException(PERSON_DOES_NOT_EXIST);

        }

        //Category
        CategoryID categoryID = CategoryID.createCategoryID(updatePersonTransactionDTO.getDenominationCategory(), personID);
        boolean categoryExistsInRepo = categoryRepository.existsById(categoryID);

        //Debit Account
        AccountID debitAccountID = AccountID.createAccountID(updatePersonTransactionDTO.getDenominationAccountDeb(), personID);
        boolean accountToDebitExistsInRepo = accountRepository.existsById(debitAccountID);

        //Credit Account
        AccountID creditAccountID = AccountID.createAccountID(updatePersonTransactionDTO.getDenominationAccountCred(), personID);
        boolean accountToCreditExistsInRepo = accountRepository.existsById(creditAccountID);

        //Ledger
        LedgerID ledgerID = person.getLedgerID();
        Optional<Ledger> optLedger = ledgerRepository.findById(ledgerID);

        if (!(categoryExistsInRepo)) {

            //"Category does not exist"
            throw new NotFoundArgumentsBusinessException(CATEGORY_DOES_NOT_EXIST);


        } else if (!(accountToDebitExistsInRepo)) {

            //"Debit Account does not exist"
            throw new NotFoundArgumentsBusinessException(ACCOUNT_DEB_DOES_NOT_EXIST);


        } else if (!(accountToCreditExistsInRepo)) {

            //"Credit Account does not exist"
            throw new NotFoundArgumentsBusinessException(ACCOUNT_CRED_DOES_NOT_EXIST);

        } else {

            Ledger ledger = optLedger.get();
            ledgerRepository.updatePersonTransaction(ledger, updatePersonTransactionDTO);

        }

        return PersonDTOAssembler.createDTOFromDomainObject(person.getPersonID().getEmail(), person.getLedgerID(), person.getName(), person.getBirthdate(), person.getBirthplace(), person.getFather(), person.getMother());
    }

    public PersonDTO deletePersonTransaction(DeletePersonTransactionDTO deletePersonTransactionDTO){

        Person person;

        PersonID personID = PersonID.createPersonID(deletePersonTransactionDTO.getEmail());
        Optional<Person> optPerson = personRepository.findById(personID);

        //If person does not exist, transaction will not be created
        if (optPerson.isPresent()) {
            person = optPerson.get();

        } else {

            throw new NotFoundArgumentsBusinessException(PERSON_DOES_NOT_EXIST);

        }

        //Ledger
        LedgerID ledgerID = person.getLedgerID();
        Optional<Ledger> optLedger = ledgerRepository.findById(ledgerID);

        Ledger ledger = optLedger.get();
        ledgerRepository.deletePersonTransaction(ledger, deletePersonTransactionDTO);

        return PersonDTOAssembler.createDTOFromDomainObject(person.getPersonID().getEmail(), person.getLedgerID(), person.getName(), person.getBirthdate(), person.getBirthplace(), person.getFather(), person.getMother());
    }

}
