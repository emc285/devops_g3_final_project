package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switch2019.project.dtoLayer.dtos.CreateGroupAccountDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IAccountRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;

import java.util.Optional;

/* US007 Como responsável do grupo, quero criar uma conta do grupo,
   atribuindo-lhe uma denominação e uma descrição, para posteriormente poder ser usada nos movimentos do grupo.
*/

/**
 * The type Us 007 create group account service.
 */

@Service
public class US007CreateGroupAccountService {

    @Autowired
    private IGroupRepository groupRepository;
    @Autowired
    private IAccountRepository accountRepository;

    /**
     * Instantiates a new Us 007 create group account service.
     *
     * @param groupRepository   the group repository
     * @param accountRepository the account repository
     */
    public US007CreateGroupAccountService(IGroupRepository groupRepository, IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        this.groupRepository = groupRepository;
    }

    /**
     * The constant SUCCESS.
     */
//Return messages
    public final static String SUCCESS = "Account created and added";
    /**
     * The constant ACCOUNT_ALREADY_EXIST.
     */
    public final static String ACCOUNT_ALREADY_EXIST = "Account already exists";
    /**
     * The constant PERSON_NOT_IN_CHARGE.
     */
    public final static String PERSON_NOT_IN_CHARGE = "Person is not in charge";
    /**
     * The constant GROUP_DOES_NOT_EXIST.
     */
    public final static String GROUP_DOES_NOT_EXIST = "Group does not exist";

    /**
     * Create account as people in charge boolean dto.
     *
     * @param createGroupAccountDTO the create group account dto
     * @return the boolean dto
     */
    public GroupDTO createAccountAsPeopleInCharge(CreateGroupAccountDTO createGroupAccountDTO) {

        Group group;

        GroupID groupID = GroupID.createGroupID(createGroupAccountDTO.getGroupDenomination());
        Optional<Group> optGroup = groupRepository.findById(groupID);

        if (!optGroup.isPresent()) {

            throw new NotFoundArgumentsBusinessException(GROUP_DOES_NOT_EXIST);

        } else {

            group = optGroup.get();

            //If Person is PeopleInCharge of a group, he/she already exists in personRepository
            PersonID personID = PersonID.createPersonID(createGroupAccountDTO.getPersonEmail());
            boolean isPeopleInCharge = group.isPersonPeopleInCharge(personID);

            AccountID accountID = AccountID.createAccountID(createGroupAccountDTO.getAccountDenomination(), groupID);
            boolean accountExistsInRepo = accountRepository.existsById(accountID);

            if (!isPeopleInCharge) {

                throw new InvalidArgumentsBusinessException(PERSON_NOT_IN_CHARGE);

            } else if (accountExistsInRepo) {

                throw new InvalidArgumentsBusinessException(ACCOUNT_ALREADY_EXIST);

            } else {

                group.addAccount(AccountID.createAccountID(createGroupAccountDTO.getAccountDenomination(), groupID));
                groupRepository.addAndSaveAccount(group, createGroupAccountDTO.getAccountDescription());

            }
        }
        return GroupDTOAssembler.createDTOFromDomainObject(group.getGroupID().getDenomination(),group.getDescription(),group.getDateOfCreation());
    }
}

