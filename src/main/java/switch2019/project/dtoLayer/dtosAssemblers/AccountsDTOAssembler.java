package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.AccountDTO;
import switch2019.project.dtoLayer.dtos.AccountsDTO;

import java.util.List;

public class AccountsDTOAssembler {

    public static AccountsDTO createDTOFromDomainObject(List<AccountDTO> accountDTOS) {

        AccountsDTO accountsDTO = new AccountsDTO(accountDTOS);

        return accountsDTO;
    }
}
