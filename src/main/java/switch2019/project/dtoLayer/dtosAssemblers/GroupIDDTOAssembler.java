package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.GroupIDDTO;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;

/**
 * The type Group iddto assembler.
 */
public class GroupIDDTOAssembler {

    /**
     * Create dto from domain object group iddto.
     *
     * @param groupID the group id
     * @return the group iddto
     */
    public static GroupIDDTO createDTOFromDomainObject(GroupID groupID) {
        GroupIDDTO groupIDDTO = new GroupIDDTO(groupID.getDenomination().getDenomination());
        return groupIDDTO;
    }
}
