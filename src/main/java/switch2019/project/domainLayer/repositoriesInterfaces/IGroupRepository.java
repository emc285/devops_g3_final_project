package switch2019.project.domainLayer.repositoriesInterfaces;

import org.springframework.stereotype.Repository;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;

import java.util.List;
import java.util.Optional;

/**
 * The interface Group repository.
 */
@Repository
public interface IGroupRepository {
    //---------------------------- NOVO --------------------------------//

    Group save( Group group );
    boolean addAndSaveAdmin(Group group, PersonID adminID);
    Optional<Group> findById(GroupID id);
    boolean addAndSaveLedger(Group group);
    List<PersonID> findAdminsById( GroupID id );
    boolean addAndSaveMember(Group group, PersonID memberID);
    boolean addAndSaveCategory(Group group);
    boolean addAndSaveAccount(Group group, String description);
    boolean exists(GroupID groupID);
    long count();
    List<Group> findAll();
}
