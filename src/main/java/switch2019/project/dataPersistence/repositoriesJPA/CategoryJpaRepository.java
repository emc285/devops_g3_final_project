package switch2019.project.dataPersistence.repositoriesJPA;

import org.springframework.data.repository.CrudRepository;
import switch2019.project.dataModelLayer.dataModel.AbstractIdJpa;
import switch2019.project.dataModelLayer.dataModel.CategoryJpa;

import java.util.List;
import java.util.Optional;

public interface CategoryJpaRepository extends CrudRepository<CategoryJpa, AbstractIdJpa> {

	List<CategoryJpa> findAll();

	Optional<CategoryJpa> findById(AbstractIdJpa id);

	boolean existsById(AbstractIdJpa id);

	long count();

	void delete(CategoryJpa categoryJpa);

	List<CategoryJpa> findAllById(AbstractIdJpa id);
}
