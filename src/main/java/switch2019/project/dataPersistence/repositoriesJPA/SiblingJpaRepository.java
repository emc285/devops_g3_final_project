package switch2019.project.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.dataModelLayer.dataModel.SiblingJpa;

import java.util.List;

public interface SiblingJpaRepository extends CrudRepository<SiblingJpa, Long> {

    SiblingJpa findById(long id);
    //List<AdminJpa> findAllByGroupId( GroupId id);
    List<SiblingJpa> findAll();
}
