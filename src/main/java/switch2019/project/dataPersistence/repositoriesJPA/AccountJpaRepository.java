package switch2019.project.dataPersistence.repositoriesJPA;

import org.springframework.data.repository.CrudRepository;
import switch2019.project.dataModelLayer.dataModel.AbstractIdJpa;
import switch2019.project.dataModelLayer.dataModel.AccountJpa;

import java.util.List;
import java.util.Optional;

public interface AccountJpaRepository extends CrudRepository<AccountJpa, AbstractIdJpa> {

	List<AccountJpa> findAll();

	Optional<AccountJpa> findById(AbstractIdJpa id);

	boolean existsById(AbstractIdJpa id);

	long count();

	void delete(AccountJpa accountJpa);

	List<AccountJpa> findAllById(AbstractIdJpa id);
}