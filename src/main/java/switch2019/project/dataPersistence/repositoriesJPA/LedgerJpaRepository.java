package switch2019.project.dataPersistence.repositoriesJPA;

import org.springframework.data.repository.CrudRepository;
import switch2019.project.domainLayer.domainEntities.vosShared.LedgerID;
import switch2019.project.dataModelLayer.dataModel.LedgerJpa;

import java.util.List;
import java.util.Optional;

public interface LedgerJpaRepository extends CrudRepository<LedgerJpa, LedgerID> {

    List<LedgerJpa> findAll();

    Optional<LedgerJpa> findById(LedgerID id);

    boolean existsById(LedgerID id);

    long count();
}