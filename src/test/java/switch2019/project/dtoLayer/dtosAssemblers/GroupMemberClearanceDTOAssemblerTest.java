package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.dtoLayer.dtos.GroupMemberClearanceDTO;

import static org.junit.jupiter.api.Assertions.*;

class GroupMemberClearanceDTOAssemblerTest {

    @Test
    @DisplayName("GroupMemberClearanceDTOAssembler - Test create GroupListDTO from domain objects")
    void test() {

        // Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        GroupMemberClearanceDTOAssembler groupMemberClearanceDTOAssembler = new GroupMemberClearanceDTOAssembler();
        GroupMemberClearanceDTO groupMemberClearanceDTO = groupMemberClearanceDTOAssembler.createDTOFromDomainObject(mariaEmail, mariaClearance);

        assertEquals(mariaEmail, groupMemberClearanceDTO.getMemberID());
        assertEquals(mariaClearance, groupMemberClearanceDTO.getClearance());
    }

}