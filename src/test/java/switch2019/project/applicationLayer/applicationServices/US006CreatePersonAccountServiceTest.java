package switch2019.project.applicationLayer.applicationServices;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.dtoLayer.dtos.CreatePersonAccountDTO;
import switch2019.project.dtoLayer.dtos.PersonDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreatePersonAccountDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.PersonDTOAssembler;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.*;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

    /*
    US 06. Como utilizador, quero criar uma conta para mim, atribuindo-lhe uma
    denominação e uma descrição, para posteriormente poder ser usada nos meus movimentos.
     */

class US006CreatePersonAccountServiceTest extends AbstractTest {

    @Mock
    private IPersonRepository personRepository;
    @Mock
    private IAccountRepository accountRepository;

    private US006CreatePersonAccountService us006CreatePersonAccountService;
    private Person person;
    private PersonID personId;

    @BeforeEach
    public void init() {

        // Manuel
        String manuelEmail = "manuel@gmail.com";
        String manuelName = "Manuel";
        LocalDate manuelBirthdate = LocalDate.of(1999, 2, 20);
        String manuelBirthplace = "Porto";
        PersonID manuelPersonID = PersonID.createPersonID(manuelEmail);

        // Ilda
        String ildaEmail = "ilda@gmail.com";
        PersonID ildaPersonID = PersonID.createPersonID(ildaEmail);

        // Paulo
        String pauloEmail = "paulo@gmail.com";
        PersonID pauloPersonID = PersonID.createPersonID(pauloEmail);

        // Helder
        String helderEmail = "helder@gmail.com";
        PersonID helderPersonID = PersonID.createPersonID(helderEmail);


        // Family Fontes
        // Accounts ->       Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services


        // 1. Person Manuel
        this.person = Person.createPerson(manuelEmail, manuelName, manuelBirthdate, manuelBirthplace);
        this.personId = PersonID.createPersonID(manuelEmail);


        // Add Accounts

        // Salary
        String accountDenomination = "Company";
        AccountID accountID = AccountID.createAccountID(accountDenomination, personId);
        person.addAccount(accountID);

        // Bank Account
        String accountBankDenomination = "Bank Account";
        AccountID accountBankID = AccountID.createAccountID(accountBankDenomination, personId);
        person.addAccount(accountBankID);

    }


    // Tests

    // Success

//    @Test
//    @DisplayName("test for createAccount() | Manuel | Success")
//    void createAccount_Success() {
//
//        // Arrange
//        String personEmail = "manuel@gmail.com";
//        String accountDescription = "Tools for equipment";
//        String accountDenomination = "Equipment";
//
//
//        // To Search
//        AccountID accountID = AccountID.createAccountID(accountDenomination, personId);
//
//        // Returning an Optional<Person> Person
//        Mockito.when(personRepository.findById(personId)).thenReturn(Optional.of(person));
//
//        // Returning False when account already exists
//        Mockito.when(accountRepository.existsById(accountID)).thenReturn(false);
//
//        // DTO
//        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail,
//                accountDescription, accountDenomination);
//
//
//        // Expected PersonDTO
//        PersonDTO expectedPersonDTO = PersonDTOAssembler.createDTOFromDomainObject(person.getEmail(), person.getName(),
//                person.getBirthdate(), person.getBirthplace(), person.getFather(), person.getMother());
//
//
//        // Act
//        US006CreatePersonAccountService us006CreatePersonAccountService = new US006CreatePersonAccountService(personRepository,
//                accountRepository);
//
//        PersonDTO result = us006CreatePersonAccountService.createAccount(createPersonAccountDTO);
//
//
//        // Assert
//        assertEquals(expectedPersonDTO, result);
//        assertEquals(true, person.checkIfPersonHasAccount(accountID));
//    }


//    // Account already added in BeforeEach
//
//    @Test
//    @DisplayName("test for createPerson() | Account already exists ")
//    void createAccount_AccountAlreadyExists() {
//
//
//        // Arrange
//        String personEmail = "manuel@gmail.com";
//        String accountDenomination = "Company";
//        String accountDescription = "Company account";
//
//        // To Search
//        AccountID accountID = AccountID.createAccountID(accountDenomination, personId);
//
//        // Returning an Optional<Person> person
//        Mockito.when(personRepository.findById(personId)).thenReturn(Optional.of(person));
//
//        // Returning True (account already exists)
//        Mockito.when(accountRepository.existsById(accountID)).thenReturn(true);
//
//        // DTO
//        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail,
//                accountDescription, accountDenomination);
//
//
//        // Act
//        US006CreatePersonAccountService us006CreatePersonAccountService = new US006CreatePersonAccountService(personRepository, accountRepository);
//        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us006CreatePersonAccountService.createAccount(createPersonAccountDTO));
//
//        // Assert
//        assertEquals(thrown.getMessage(), US006CreatePersonAccountService.ACCOUNT_ALREADY_EXIST);
//    }
//
//
//    // Person does not exist - not created in BeforeEach()
//
//    @Test
//    @DisplayName("test for createAccount() | Person does not exist")
//    void createAccount_PersonDoesNotExist() {
//
//        // Arrange
//        String personEmail = "santi@gmail.com";
//        String accountDenomination = "Company";
//        String accountDescription = "Company account";
//
//        // To Search
//        AccountID accountID = AccountID.createAccountID(accountDenomination, personId);
//
//        // Returning an Optional<Person> person
//        Mockito.when(personRepository.findById(personId)).thenReturn(Optional.of(person));
//
//        // Returning True (account already exists)
//        Mockito.when(accountRepository.existsById(accountID)).thenReturn(true);
//
//        // DTO
//        CreatePersonAccountDTO createPersonAccountDTO = CreatePersonAccountDTOAssembler.createDTOFromPrimitiveTypes(personEmail,
//                accountDescription, accountDenomination);
//
//        // Act
//        US006CreatePersonAccountService us006CreatePersonAccountService = new US006CreatePersonAccountService(personRepository, accountRepository);
//        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us006CreatePersonAccountService.createAccount(createPersonAccountDTO));
//
//        // Assert
//        assertEquals(thrown.getMessage(), US006CreatePersonAccountService.PERSON_DOES_NOT_EXIST);
//    }


}
