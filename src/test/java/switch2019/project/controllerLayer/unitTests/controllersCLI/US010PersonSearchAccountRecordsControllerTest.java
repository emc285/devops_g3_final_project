package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US010PersonSearchAccountRecordsService;
import switch2019.project.dtoLayer.dtos.PersonSearchAccountRecordsInDTO;
import switch2019.project.dtoLayer.dtos.SearchAccountRecordsOutDTO;
import switch2019.project.dtoLayer.dtosAssemblers.PersonSearchAccountRecordsInDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.SearchAccountRecordsOutDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US010PersonSearchAccountRecordsController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class US010PersonSearchAccountRecordsControllerTest extends AbstractTest {

    @Mock
    private US010PersonSearchAccountRecordsService us010Service;

    @Test
    @DisplayName("Test for controller method getPersonAccountTransactionsWithinPeriod() - Success")
    void getAccountTransactionsWithinPeriod_success() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-03-03";
        String endDate = "2020-04-03";

        //Arrange category and accounts for expected transactions
        //Category Electricity Expenses
        String denominationCategoryEDP = "Electricity Expenses";
        CategoryID categoryIDEdp = CategoryID.createCategoryID(denominationCategoryEDP, PersonID.createPersonID(personEmail));

        //Account EDP
        AccountID accountIDEdp = AccountID.createAccountID(accountDenomination, PersonID.createPersonID(personEmail));

        //Account Wallet - House expenses
        String walletAccountDenomination = "House Wallet Funds";
        AccountID accountIDWallet = AccountID.createAccountID(walletAccountDenomination, PersonID.createPersonID(personEmail));

        //Arrange expected transactions
        //Transaction 2 - EDP - Debit
        String typeTransaction2 = "Debit";
        String descriptionTransaction2 = "EDP bill from February/2020";
        LocalDate dateTransaction2 = LocalDate.of(2020, 03, 03);
        double amountTransaction2 = 45.00;
        Transaction transaction2 = Transaction.createTransaction(categoryIDEdp, typeTransaction2, descriptionTransaction2, amountTransaction2, dateTransaction2, accountIDWallet, accountIDEdp);

        //Transaction 3 - EDP - Credit
        String typeTransaction3 = "Credit";
        String descriptionTransaction3 = "EDP bill from March/2020 - settlement - overcharge";
        LocalDate dateTransaction3 = LocalDate.of(2020, 04, 03);
        double amountTransaction3 = 15.00;
        Transaction transaction3 = Transaction.createTransaction(categoryIDEdp, typeTransaction3, descriptionTransaction3, amountTransaction3, dateTransaction3, accountIDEdp, accountIDWallet);

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Expected DTO out
        ArrayList<Transaction> expectedTransactions = new ArrayList<>();
        expectedTransactions.add(transaction2);
        expectedTransactions.add(transaction3);

        SearchAccountRecordsOutDTO expectedDTOout = SearchAccountRecordsOutDTOAssembler.accountTransactionsOutDTO(expectedTransactions);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenReturn(expectedDTOout);

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        SearchAccountRecordsOutDTO result = us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate);

        //ASSERT
        assertEquals(expectedDTOout, result);
    }

    @Test
    @DisplayName("Test for controller method getPersonAccountTransactionsWithinPeriod() - Exception - No transactions to report")
    void getAccountTransactionsWithinPeriod_exception_noTransactionsToReport() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-02-04";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010PersonSearchAccountRecordsService.NO_TRANSACTIONS_TO_REPORT));

        //Expected message
        String expectedMessage = "Ledger has no transactions within the searched period";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getPersonAccountTransactionsWithinPeriod() - Fail - Person does not exist")
    void getAccountTransactionsWithinPeriod_fail_personDoesNotExist() {
        //ARRANGE
        //Person to search
        String personEmail = "isolina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-02-04";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new InvalidArgumentsBusinessException(US010PersonSearchAccountRecordsService.PERSON_DOES_NOT_EXIST));

        //Expected message
        String expectedMessage = "Person does not exist in the system";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getPersonAccountTransactionsWithinPeriod() - Fail - Account does not exist")
    void getAccountTransactionsWithinPeriod_fail_accountDoesNotExist() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-02-04";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new InvalidArgumentsBusinessException(US010PersonSearchAccountRecordsService.ACCOUNT_DOES_NOT_EXIST));

        //Expected message
        String expectedMessage = "Account does not exist in the system";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getPersonAccountTransactionsWithinPeriod() - Fail - Search dates in reverse order")
    void getAccountTransactionsWithinPeriod_fail_startDateAfterEndDate() {
        //ARRANGE
        //Person to search
        String personEmail = "isolina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-04-04";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new InvalidArgumentsBusinessException(US010PersonSearchAccountRecordsService.DATES_IN_REVERSE_ORDER));

        //Expected message
        String expectedMessage = "Check the start and end dates for the period, since start date cannot be later than end date";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getPersonAccountTransactionsWithinPeriod() - Exception - Empty ledger")
    void getAccountTransactionsWithinPeriod_exception_emptyLedger() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-02-04";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010PersonSearchAccountRecordsService.EMPTY_LEDGER));

        //Expected message
        String expectedMessage = "Ledger is empty";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getPersonAccountTransactionsWithinPeriod() - Exception - Search period before ledger records time range")
    void getAccountTransactionsWithinPeriod_exception_searchDatesBeforeLedgerRecords() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-02-04";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010PersonSearchAccountRecordsService.TIME_PERIOD_OUTSIDE_OF_RECORDS_RANGE));

        //Expected message
        String expectedMessage = "The time period provided falls outside the range of the ledger records";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getAccountID() - Exception - Account name missing")
    void getAccountID_exception_accountNameMissing() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "";

        //Dates to search
        String startDate = "2020-02-04";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010PersonSearchAccountRecordsService.ACCOUNT_NAME_FIELD_MISSING));

        //Expected message
        String expectedMessage = "Search results cannot be displayed: account name is missing";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getStartDate() - Exception - Start date missing")
    void getStartDate_exception_startDateMissing() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "";
        String endDate = "2020-03-02";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010PersonSearchAccountRecordsService.START_DATE_FIELD_MISSING));

        //Expected message
        String expectedMessage = "Search results cannot be displayed: start date is missing";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    @DisplayName("Test for controller method getEndDate() - Exception - End date missing")
    void getEndDate_exception_endDateMissing() {
        //ARRANGE
        //Person to search
        String personEmail = "joaquina@gmail.com";

        //Account to search - EDP
        String accountDenomination = "EDP";

        //Dates to search
        String startDate = "2020-02-04";
        String endDate = "";

        //Arrange DTO in
        PersonSearchAccountRecordsInDTO dtoIn = PersonSearchAccountRecordsInDTOAssembler.personAccountTransactionsInDTO(personEmail, accountDenomination, startDate, endDate);

        //Mock the behaviour of the service method getPersonAccountTransactionsWithinPeriod,
        //so it does not depend on other parts (e.g. DB)
        Mockito.when(us010Service.getPersonAccountTransactionsWithinPeriod(dtoIn)).thenThrow(new NotFoundArgumentsBusinessException(US010PersonSearchAccountRecordsService.END_DATE_FIELD_MISSING));

        //Expected message
        String expectedMessage = "Search results cannot be displayed: end date is missing";

        //ACT
        US010PersonSearchAccountRecordsController us010PersonController = new US010PersonSearchAccountRecordsController(us010Service);
        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us010PersonController.getPersonAccountTransactionsWithinPeriod(personEmail, accountDenomination, startDate, endDate));

        //ASSERT
        assertEquals(expectedMessage, thrown.getMessage());
    }

}