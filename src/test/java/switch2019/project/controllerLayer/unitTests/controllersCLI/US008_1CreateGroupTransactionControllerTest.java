package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US008_1CreateGroupTransactionService;
import switch2019.project.dtoLayer.dtos.*;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupTransactionDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.controllerLayer.controllers.controllersCLI.US008_1CreateGroupTransactionController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

class US008_1CreateGroupTransactionControllerTest extends AbstractTest {

    @Mock
    private US008_1CreateGroupTransactionService service;


    //TESTS
    @Test
    @DisplayName("Test for service method createGroupTransaction() - Success")
    void createGroupTransaction_success() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";


        String denominationCategory = "IRS";
        String type = "debit";
        String transactionDescription = "May IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = "2020-06-18";


        //Expected result
        Denomination denomination = Denomination.createDenomination(groupDenomination);
        Description description = Description.createDescription(groupDescription);
        DateOfCreation dateOfCreation = DateOfCreation.createDateOfCreation(LocalDate.now());
        GroupDTO isTransactionCreatedExpected = GroupDTOAssembler.createDTOFromDomainObject(denomination, description, dateOfCreation);

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createGroupTransaction(createGroupTransactionDTO)).thenReturn(isTransactionCreatedExpected);

        //Controller
        US008_1CreateGroupTransactionController controller = new US008_1CreateGroupTransactionController(service);

        //Act

        GroupDTO result = controller.createGroupTransaction(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date);

        //Assert
        assertEquals(isTransactionCreatedExpected, result);
    }

    @Test
    @DisplayName("Test for service method createGroupTransaction() - Fail (group does not exist)")
    void createGroupTransaction_failGroupDoesNotExist() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Vale Family";
        String groupDescription = "All members from Fontes family";


        String denominationCategory = "IRS";
        String type = "debit";
        String transactionDescription = "May IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = "2020-06-18";

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createGroupTransaction(createGroupTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008_1CreateGroupTransactionService.GROUP_DOES_NOT_EXIST));

        //Controller
        US008_1CreateGroupTransactionController controller = new US008_1CreateGroupTransactionController(service);

        //Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createGroupTransaction(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date));

        //Assert
        assertEquals(thrown.getMessage(), US008_1CreateGroupTransactionService.GROUP_DOES_NOT_EXIST);
    }

    @Test
    @DisplayName("Test for service method createGroupTransaction() - Fail (person is not a member)")
    void createGroupTransaction_failPersonIsNotMember() {
        //Arrange

        String personEmail = "ricardo@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";


        String denominationCategory = "IRS";
        String type = "debit";
        String transactionDescription = "May IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = "2020-06-18";

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createGroupTransaction(createGroupTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008_1CreateGroupTransactionService.PERSON_NOT_MEMBER));

        //Controller
        US008_1CreateGroupTransactionController controller = new US008_1CreateGroupTransactionController(service);

        //Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createGroupTransaction(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date));

        //Assert
        assertEquals(thrown.getMessage(), US008_1CreateGroupTransactionService.PERSON_NOT_MEMBER);
    }

    @Test
    @DisplayName("Test for service method createGroupTransaction() - Fail (category is not created)")
    void createGroupTransaction_failCategoryIsNotCreated() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";


        String denominationCategory = "Pets";
        String type = "debit";
        String transactionDescription = "May IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "State";
        String date = "2020-06-18";

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createGroupTransaction(createGroupTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008_1CreateGroupTransactionService.NEED_TO_CREATE_CATEGORY));

        //Controller
        US008_1CreateGroupTransactionController controller = new US008_1CreateGroupTransactionController(service);

        //Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createGroupTransaction(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date));

        //Assert
        assertEquals(thrown.getMessage(), US008_1CreateGroupTransactionService.NEED_TO_CREATE_CATEGORY);
    }

    @Test
    @DisplayName("Test for service method createGroupTransaction() - Fail (account to credit is not created)")
    void createGroupTransaction_failCreditAccountIsNotCreated() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";


        String denominationCategory = "IRS";
        String type = "debit";
        String transactionDescription = "May IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Vet";
        String denominationAccountCred = "State";
        String date = "2020-06-18";

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createGroupTransaction(createGroupTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008_1CreateGroupTransactionService.NEED_TO_CREATE_ACCOUNT_TO_CREDIT));

        //Controller
        US008_1CreateGroupTransactionController controller = new US008_1CreateGroupTransactionController(service);

        //Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createGroupTransaction(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date));

        //Assert
        assertEquals(thrown.getMessage(), US008_1CreateGroupTransactionService.NEED_TO_CREATE_ACCOUNT_TO_CREDIT);
    }

    @Test
    @DisplayName("Test for service method createGroupTransaction() - Fail (account to debit is not created)")
    void createGroupTransaction_failDebitAccountIsNotCreated() {
        //Arrange

        String personEmail = "manuel@gmail.com";
        String groupDenomination = "Fontes Family";
        String groupDescription = "All members from Fontes family";


        String denominationCategory = "IRS";
        String type = "debit";
        String transactionDescription = "May IRS";
        double amount = 150.0;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "Vet";
        String date = "2020-06-18";

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date);

        // Mock the behaviour of the service's createCategoryAsPersonInCharge method,
        // so it does not depend on other parts (e.g. DB)
        Mockito.when(service.createGroupTransaction(createGroupTransactionDTO)).thenThrow(new NotFoundArgumentsBusinessException(US008_1CreateGroupTransactionService.NEED_TO_CREATE_ACCOUNT_TO_DEBIT));

        //Controller
        US008_1CreateGroupTransactionController controller = new US008_1CreateGroupTransactionController(service);

        //Act

        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> controller.createGroupTransaction(groupDenomination, personEmail, denominationCategory, denominationAccountDeb, denominationAccountCred, amount, type, transactionDescription, date));

        //Assert
        assertEquals(thrown.getMessage(), US008_1CreateGroupTransactionService.NEED_TO_CREATE_ACCOUNT_TO_DEBIT);
    }

}