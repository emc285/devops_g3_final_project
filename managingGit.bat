cls
@echo Welcome to managingGit tool
@echo.
@echo First of all let's see what are the status of your repository..
@echo.
:start
 git status
@echo.
@echo What operation would you like to do? (after typing your option please press ENTER)
@echo.
@echo    1  - See differences
@echo    2  - Add a file
@echo    3  - Add all files
@echo    4  - Commit all files with message
@echo    5  - Push to repository
@echo    6  - Pull from repository
@echo    7  - Restore/unstage file
@echo    8  - Commit history
@echo    9  - Git branch
@echo    0  - Exit
@echo    S  - Git status
@echo    T  - Type a command
@echo  TAG  - Tag management
@echo  MTAG - Merge Tag
@echo  CTAG - Commit Tag

@echo.
@echo ______________________________________________________
@echo.
@set /p PROGRAM= What do you want to do?

@goto %PROGRAM%

:1
@REM See differences
cls
git diff --staged
@echo.
@echo ______________________________________________________
@echo.
@pause
@echo.
@goto start


REM ____________________________________________________________________________________________________________
:2
@REM Add a file
@echo  If you'd like to go back please type 1.
@echo.

@set /p FILE= What file would you like to add (please type the complete description of the file)?

if "%FILE%" =="1" (@goto start)

git add %FILE%
@goto start

REM ____________________________________________________________________________________________________________
:3
@REM Add all files

 git add .
cls
@goto start

REM ____________________________________________________________________________________________________________
:4
@REM Commit all files with message
@echo  If you'd like to go back please type 1.
@echo.

@set /p MESSAGE= What message would you like to add on your commit(please type the complete message)?
if "%MESSAGE%" =="1" (@goto start)
git commit -am "%MESSAGE%"

@goto start

REM ____________________________________________________________________________________________________________
:5
@REM Push to repository
git push

@echo.
@echo ___________________________________________________________________________________________________________
@echo.

@goto start

REM ____________________________________________________________________________________________________________
:6
git pull
@echo.
@goto start

REM ____________________________________________________________________________________________________________
:7
@REM Restore/unstage/unchached file
@echo  If you'd like to go back please type 1.
@echo.
@set /p OPTION= Would you like to unstage (type U), discard changes (type DC) or cache remove (type RC) a file in the PWD?
if "%OPTION%" =="1" (@goto start)
@goto %OPTION%

:U
@REM unstage files
@set /p FILETOUNSTAGE= What file would you like to unstage (please type the complete filename)?

git restore --staged %FILETOUNSTAGE%
cls
@goto start

:DC
@REM discard changes
@set /p FILETORESTORE= What file would you like to restore (please type the complete filename)?

git restore %FILETORESTORE%
cls
@goto start

:RC
@REM remove cached files
@set /p REMOVECACHEDFILE= What file would you like to unstage (please type the complete filename)?

git rm --cached %REMOVECACHEDFILE%
cls
@goto start


REM ___________________________________________________________________________________________________________
:8
@REM Commit history
git log
@pause
cls
@goto start

@REM ____________________________________________________________________________________________________________
:9
@REM git branch
@echo.
:branchStart
git branch
@echo.
@echo ______________________________________________________
@echo.
@echo  What operation would you like to do?
@echo  1- Create branch
@echo  2- Change branch
@echo  3- Merge
@echo  4- Delete branch
@echo  5- Back
@echo  6- exit
@echo.
@echo ______________________________________________________
@echo.
@set /p OPERATION= What operation would you like to do?

@goto %OPERATION%

REM ____________________________________________________________________________________________________________
:1
@echo.
@echo  If you'd like to go back please type 1.
@echo.
@set /p NEWBRANCH= Please type the name of the branch that you want to create?
if "%NEWBRANCH%" =="1" (@goto branchStart)
git branch %NEWBRANCH%


REM ____________________________________________________________________________________________________________
echo.
cls
@echo.
@echo ______________________________________________________
@echo.
@goto branchStart
@echo.
@echo ______________________________________________________
@echo.

REM ____________________________________________________________________________________________________________
:2
@echo.
git branch
@echo.
@echo ______________________________________________________
@echo.
@echo  If you'd like to go back please type 1.
@echo.
@set /p BRANCHTOCHANGE= Please type the name of the branch that you want to change to?
if "%BRANCHTOCHANGE%" =="1" (@goto branchStart)
git checkout %BRANCHTOCHANGE%

@goto branchStart
REM ____________________________________________________________________________________________________________
:3
@echo To do so we need to change to the master branch.
@echo.
@pause
git checkout master
@echo.
@echo  If you'd like to go back please type 1.
@echo.

@set /p MERGEBRANCHES= Please type the name of the branch that you want to merge with?
if "%MERGEBRANCHES%" =="1" (@goto branchStart)
cls
git merge %MERGEBRANCHES%
@goto branchStart


REM ____________________________________________________________________________________________________________
:4
@echo  If you'd like to go back please type 1.
@echo.
@set /p BRANCHTODELETE= Please type the name of the branch that you want to delete?
if "%BRANCHTODELETE%" =="1" (@goto branchStart)

git branch -d %BRANCHTODELETE%
@goto branchStart

REM ____________________________________________________________________________________________________________
:5
cls
@goto start

REM ____________________________________________________________________________________________________________
:6
cls
@exit /b

REM ____________________________________________________________________________________________________________
:0
@exit /b

REM ____________________________________________________________________________________________________________

:S
cls

@echo.
@goto start

REM ____________________________________________________________________________________________________________

:T
@echo  If you'd like to go back please type 1.
@echo.
@set /p COMMAND= Please type the command that you want to run?
if "%COMMAND%" =="1" (@goto start)
call %COMMAND%

@goto start

REM ____________________________________________________________________________________________________________

:TAG
REM Tag commits

:tagstart
git tag
@echo.
@set /p OPTION= If you'd like to continue just press 1, if you want to remove a tag please type 2 and ^
 if you'd like to go back please type 3.
@echo.
@goto %OPTION%

:1
@set /p TAG= Please type the name of the tag that you want to add?
@set /p TAGMESSAGE= What message would you like to add on tag (please type the complete message)?

git tag -a %TAG% -m "%TAGMESSAGE%"
git push origin %TAG%
cls
@goto tagstart

:2
@set /p TAGTOREMOVE= Please type the name of the tag that you want to remove?
git tag -d %TAGTOREMOVE%
cls
@goto tagstart

:3
cls
@goto start
REM ____________________________________________________________________________________________________________

:MTAG
@echo To do so we need to change to the master branch.
@echo.
@pause
git checkout master
@echo.
@echo  If you'd like to go back please type 1.
@echo.
git branch
@echo.
@set /p MERGEBRANCHES= Please type the name of the branch that you want to merge with?
if "%MERGEBRANCHES%" =="1" (@goto branchStart)
cls
call git merge %MERGEBRANCHES%

call git push

@set /p TAG= Please type the name of the tag that you want to add?
@set /p TAGMESSAGE= What message would you like to add on tag (please type the complete message)?


call git tag -a %TAG% -m "%TAGMESSAGE%"
call git push origin %TAG%

@goto start


:CTAG
@REM Commit all files with message
@echo  If you'd like to go back please type 1.
@echo.

@set /p MESSAGE= What message would you like to add on your commit(please type the complete message)?
if "%MESSAGE%" =="1" (@goto start)
call git commit -am "%MESSAGE%"

call git push

@set /p TAG= Please type the name of the tag that you want to add?
@set /p TAGMESSAGE= What message would you like to add on tag (please type the complete message)?


call git tag -a %TAG% -m "%TAGMESSAGE%"
call git push origin %TAG%
@goto start
