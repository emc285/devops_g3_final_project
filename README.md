# Apply DevOps to the Personal Finance Project
  
This technical report was developed in context of *DevOps* discipline, belonging to the *2019-2020 SWitCH Program* from *I.S.E.P.*.
    
The main **goal** of the present assignment is to **implement a pipeline in Jenkins** for the **Personal Finance Project** and apply the concerns from DevOps such as:

  - Infrastructure as Code
  - Bitbucket / Git 
  - Vagrant
  - Build Tools
  - Build Stages of a Pipeline
  
![](Images/DEVOPS.JPG)

**PREVIOUS REQUIREMENTS:**
  
In order to carry out the requested tasks, it was necessary to create **two forks** of the main project, **"Personal Finance Management"** application:

 - The [devops_g3_final_project](https://bitbucket.org/emc285/devops_g3_final_project/src/master/) Fork » Will incorporate all the development to implement a pipeline in Jenkins and apply the DevOps concerns. 
 
 - The [devops_g3_alternative](https://bitbucket.org/emc285/devops_g3_alternative/src/master/) Fork » Will incorporate all the development to implement a pipeline in Jenkins and apply alternative tools to those that were implemented in the main fork (devops_g3_final_project).
 
To do a fork we select the original project 'switch2019_g3' in *Bitbucket* remote repository and, after click on `+` in the side banner, we must define the following settings:

  - **Owner**: the owner was defined by default the person who created it.
  - **Name**: we define the name of the fork.
  - **Description**: we don't put any description ... but we can still do it.
  - **Access level**: ours is currently private.
  - **Users**: all members of the group with administrator permissions.

After all settings have been defined, we just need press the button `Create Fork`.
    
## 1 | Infrastructure as Code

Infrastructure as code is the **process of managing and provisioning data processing centers using configuration files** instead of physical hardware configurations or interactive configuration tools.
  
The main feature of infrastructure as code is the use of declarative scripts or definitions, instead of manual processes.
  
## 2 | Bitbucket and Git
 
### Working with Bitbucket


**Bitbucket** is a **web-based version control repository hosting service**, for source code and development projects that use Git revision control systems.

In the beginning of assignment had been created the following main issues that represent the parcelling of the problem or challenge.
  
**Main Tasks / Issues**

  * First Commit - Create 'Devops' Folder
  * Add the script for managing git
  * Update project source
  * Creation and development of a Vagrantfile
  * Creation and development of a playbook.yml File
  * Development of the dockerfiles for the creation of web and database images
  * Creation and development of a Jenkinsfile
  * Create a README.md file for all project

### Prepare the work directory

To start the development of DevOps concepts on the Personal Finance Project we need to change to the main **fork** previously created: **devops_g3_final_project**.

Executing the following command line, we change the work directory to the folder of fork:

```
$ cd "C:\Users\USER\Desktop\devops_g3_final_project"
```  

Already on the folder of fork we should create a main directory 'devops' and start to working on it.
  
To create the directory with a simple file to commit and to add it to the remote repository we execute:

```
$ mkdir devops
$ cd "C:\Users\USER\Desktop\devops_g3_final_project\devops"
$ echo files>>allFilesHere
$ git commit -m "close #1 - Create 'Devops' Folder"
$ git push
```

### Working with GIT
  
**Git** is a distributed **version-control system** for tracking changes in source code, during software development.
  
This tool has the functionality, performance, security and flexibility that most developers needs to work individually or in a team.
As a starting challenge,  Group 3 decided to develop a script named **ManagingGit.bat**, with the intent of speed up multiple processes of the GIT tool.
  
As it is possible to see at the following image, the command line `git status` is executed automatically when we run the *ManagingGit.bat*.
  
This command will present to user the status of the repository (files to add/ files not staged).
  
![](Images/ManagingGitMainMenu.PNG)
  
### How this script works?

  * **Typing 1 and Enter**
    
    It will run the command `git diff --staged`. 
    The user can see all the differences regarding the files that we can add in the next commit.

  * **Typing 2 and Enter**
  
    It will run the command `git add`.     
    The user are asked which file he want to add to the next commit.

  * **Typing 3 and Enter**
  
    It will run the command `git add .`.     
    All the files in the current directory will be added but not any of subdirectories.
    
  * **Typing 4 and Enter**
  
    It will run the command `commit -am [message]`.     
    The user will have access to commit all the files previously added .
  
  * **Typing 5 and Enter**
  
    It will run the command `git push`.
    
  * **Typing 6 and Enter**

    It will run the command `git pull`.

  * **Typing 7 and Enter**
  
    The user will be redirected to a **submenu**, and will be enabled to **manage all the files and as well as its changes**.
    
    ![](Images/ManagingGit_7.PNG)
   
    Having in consideration the option selected, the user can:
      
      - **Unstaging a file**, typing key **U**
      - It will run the command `git restore --staged <FileToRestore>`.       
      - **Remove a file**, typing keys **RC**
      - It will run the command `git rm --cached <REMOVECACHEDFILE>`.       
      - **Discard the changes of a file**, typing keys **DC**
      - It will run the command `git restore <FILETORESTORE>`.       
    
    (Please be advised that the important thing is the letters typed, not if they are in lowercase or uppercase)       
    
    After selecting the option the script will ask the file name.

  * **Typing 8 and Enter**
  
    It will run the command `git log`.    
    The user can check all the commit history.
    
  * **Typing 9 and Enter**
    
    It will run the command `git branch`.
    
    The user will be redirected to a **submenu** and will be enabled to **manage all the options related to branches** as shown in the image below.
        
    ![](Images/ManagingGit_9.PNG)
    
    Having in consideration the option selected, the user can:  
 
       -  **Create a new branch**, pressing key number **1**, to run `git branch %NEWBRANCH%`.       
          - In this step will be requested: the name for the new branch.
          
       - **Change branch**, pressing key number **2**, to run `git checkout %BRANCHTOCHANGE%`.
          - In this step will be requested: the name of the branch intended to start to work on.
          
       - **Merge branches**, pressing key number **3**, to run the command `git merge %MERGEBRANCHES%`.
          - In this step will be requested: the name of the branch intended to merge with the master branch.
          
       - **Delete a branch**, pressing key number **4**, to run the command `git branch -d %BRANCHTODELETE%`.
          - In this step will be requested: the name of the branch intended to delete.
          
       - **Back to main menu**, pressing key number **5**, to run the command `@goto start` and go to the previous menu (main menu). 
             
       - **Exit the script**, pressing key number **6**, to run the command `exit /b` and to exit the script. 
  
  * **Typing T and Enter**
      
    The user will be able to type a command line if the intention is to write a command and not a script.    
    In this step the user will be requested to type the command to run.
  
  * **Typing TAG and Enter**
  
    The user will be redirected to a **submenu** and will be enabled to **manage all the options related to tags** as shown in the image below.
  
    ![](Images/ManagingGit_tag.jpeg)
    
    Having in consideration the option selected, the user can:
        
     - **Create a new tag**, pressing key number **1**, to run the command `git tag -a %TAG% -m %TAGMESSAGE%`.     
       - In this step will be requested: the name and the message to the new tag;  
       Once the tag is created the script will run automatically the command `git push origin %TAG%`.       
       
     - **Remove a tag**, pressing key number **2**, to run the command `git tag -d %TAGTOREMOVE%`.
       - In this step will be requested: the name of the tag to delete.       
              
     - **Back to main menu**, pressing key number **3**.
          
  * **Typing MTAG and Enter**
    
     It will run some steps in order to merge two branches, pushing the changes, and add a tag to this push.
       
     The script will **execute automatically** the following **command lines**:
     
       - **1st**: Command `git merge %MERGEBRANCHES%`
           - In this step will be requested: the name of the branch that to merge with.           
       - **2nd**: Command `git push`       
       - **3rd**: Command `git tag -a %TAG% -m %TAGMESSAGE%`
           - In this step will be requested: the tag name and the tag description.           
       - **4th**: Command `git push origin %TAG%`

  * **Typing CTAG and Enter**
  
    It will run some steps in order to commit and push files previously added and add a tag to this push.
    Before execute this command line, the user needs to add the desired files to commit.
    
    The script will **execute automatically** the following **command lines**:
         
       - **1st**: Command `git commit -am <MESSAGE>`       
         - In this step will be requested: the message / description to that commit.
       - **2nd**: Command `git push`  
       - **3rd**: Command `git tag -a %TAG% -m %TAGMESSAGE%`
         - In this step will be requested: the tag name and the tag description.
       - **4th**: `git push origin %TAG%`.
  
  
### Create a new branch 'gitScript'
  
Intending to create and add the git script to the Personal Finance Project we create an independent line of development (branch), executing:
  
 ```
 $ git checkout -B gitScript
 ```

To add the script *managingGit.bat* to the remote repository and merge the branch 'gitScript' with branch 'master' we execute:

```
$ git add managingGit.bat
$ git commit -a -m "close #2 - Git Script developed to automatize the git workflow"
$ git push -u origin gitScript
$ git checkout master
$ git merge gitScript
$ git push
```
  
## 3 | Using Vagrant to Virtualizing the parts of solution
  
**Vagrant** is a tool to **build and manage Virtual Machine environments** in a single workflow.

Using a simple configuration file, called by Vagrantfile, Vagrant allows to define all of the configuration of one or more VMs .
  
Thus, become it so easy to reproduce environments that we can even say that sharing a VM is sharing a Vagrantfile.

In this task we will use Vagrant to virtualize all the parts of the solution:

 - One VM for running the database server PostgreSQL;
 - One  VM for running the web application;
 - One VM for running Jenkins and Ansible.

### Create a new branch 'vagrantfile'
  
Intending to create and add the vagrantfile to the Personal Finance Project we create an independent line of development (branch), executing:
  
```
$ git checkout -B vagrantfile
```

### Update the spring boot application for using PostgreSQL as the DBMS (DataBase Management System)

**1 |** As a first approach, all references to the current DBMS, H2 (H2 is an open-source lightweight Java database), were removed from the source code/tests.

**2 |** The _pom.xml_ was edited in order to remove the H2 database dependency, which was replaced by the PostgreSQL dependency:

[pom.xml](https://bitbucket.org/emc285/devops_g3_final_project/src/master/pom.xml)

```

    <dependency>
    <groupId>org.postgresql</groupId>
    <artifactId>postgresql</artifactId>
    <scope>runtime</scope>
    </dependency>

```

**3 |** The project's application.properties file was rewritten, in order to connect to the PostgreSQL server:

[application.properties](https://bitbucket.org/emc285/devops_g3_final_project/src/master/src/main/resources/application.properties)

```

    spring.main.banner-mode=off
    logging.level.org.springframework=ERROR
    spring.jpa.hibernate.ddl-auto=create-drop
    spring.datasource.initialization-mode=always
        
    spring.datasource.platform=postgres
    spring.datasource.url=jdbc:postgresql://192.168.123.12:5432/testdb
    spring.datasource.username=user_devops
    spring.datasource.password=supers3cret
        
    spring.jpa.show-sql=true
    spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
    spring.shell.interactive.enabled=false
        
    spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults=false
    spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL95Dialect
    spring.datasource.driver-class-name=org.postgresql.Driver

```
 
### Development of the Vagrantfile
  
**General provisioning for all Virtual Machines**

**1 |** The following packages were identified as required for all the 3 Virtual Machines:

  - git: selected as the distributed revision control system;

  - nano: selected for text editing;

  - iputils-ping: Tools to test the reachability of network hosts (tool to ensure the VMs can communicate with one another)

  - python3: to provide support for Ansible, on both the Control Machine and the Managed Nodes.

**2 |**The general provisions for all VMs foreseen on the Vagrantfile are:

[Vagrantfile](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/Vagrantfile)

```

    Vagrant.configure('2') do |config|
    config.vm.box = 'ubuntu/bionic64'
       
    config.vm.provision 'shell', inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install git -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install python3 -y
    sudo apt-get install nano -y
    SHELL

```

### Provisioning for the PostegreSQL Database VM (VM hostname:'g3db')

**1 |** The first VM to be built (the first one configured on the Vagrantfile) is the one holding the database (`g3db`); the database is usually booted before the web server (`g3web`), since the web server, when it is launched, it will need to access the database.

**2 |** The configuration of the VM `g3db` includes:
  
  - IPv4 address assigned to the VM was: 192.168.123.12;

  - The Port 5432 on the VM was forwarded to Port 5432 on the host machine;
    
  - The Vagrant box ubuntu/bionic64 was installed on the VM

  - The following script was developed for the remaining provisions, in order to:
        
    - install PostgreSQL
        
    - fix permissions on pg_hba.conf and postgresql.conf to listening to all IP addresses
        
    - create a user ('user_devops'), create the database ('testdb') and ensuring the _postgres service_ is running.

**3 |** The provisions for the `g3db` VM foreseen on the Vagrantfile were:

[Vagrantfile](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/Vagrantfile)

```
 
    config.vm.define 'g3db' do |g3db|
        g3db.vm.box = 'ubuntu/bionic64'
        g3db.vm.hostname = 'g3db'
        g3db.vm.network 'private_network', ip: '192.168.123.12'

        g3db.vm.network 'forwarded_port', guest: 8080, host: 8082
        g3db.vm.network 'forwarded_port', guest: 5432, host: 5432

        g3db.vm.provision 'shell', inline: $scriptDB
    end

    $scriptDB = <<-'SCRIPT'
        echo "-------------------- updating package lists"
        sudo apt-get update -y
        echo "-------------------- installing PostgreSQL"
        sudo apt-get install wget ca-certificates -y
        wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
        sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
        # Update the package list to ensure the latest PostgreSQL package is being installed
        sudo apt update -y
        sudo apt -y install postgresql postgresql-contrib
        echo "-------------------- fixing permissions on pg_hba.conf and postgresql.conf"
        sudo sed -i "s/#listen_address.*/listen_addresses = '*'/" /etc/postgresql/12/main/postgresql.conf
        sudo sed -i '/^# IPv4 local connections.*/a host    all             all             0.0.0.0/0               trust' /etc/postgresql/12/main/pg_hba.conf
        echo "-------------------- creating PostgreSQL user with full permissions"
        sudo su postgres -c "psql -c \"CREATE USER user_devops WITH PASSWORD 'supers3cret'\" "
        echo "-------------------- creating PostgreSQL database testdb"
        sudo su postgres -c "createdb -E UTF8 -T template0 --locale=C.UTF-8 -O user_devops testdb"
        echo "-------------------- upgrading packages to latest version"
        sudo apt-get upgrade -y
        echo "-------------------- restarting postgres service"
        cd /etc/postgresql/12/main
        sudo service postgresql restart
    SCRIPT

```

### Provisioning for the web application VM (VM hostname: 'g3web')

**1 |** The `g3web` VM holds the application files required for building the JAR file of the application and rendering the User Interface, thus running the frontend and backend concurrently.

**2 |** The configuration of the VM `g3web` includes:

  - IPv4 address assigned to the VM was: 192.168.123.11;

  - The Port 8080 on the VM was forwarded to Port 8080 on the host machine, for exposing the Spring Boot's embedded Tomcat;

  - The Port 3000 on the VM was forwarded to Port 3000 on the host machine, for rendering the User Interface;
    
  - the Vagrant box ubuntu/bionic64 was installed on the VM

  - The VM as setup with 2048 RAM for supporting the build of the application;

  - The VM was provisioned for always running the application's JAR file upon booting;

  - A script was developed for the remaining provisions, in order to:
    
    - install JDK 8 (Java Development Kit), Maven, Node and NPM (Node Package Manager)

    - setup environmental variables globally for JAVA_HOME (JDK 8), MAVEN_HOME (for Maven 1) and M2_HOME (for Maven 2 and later)
        
    - remove the Tomcat8 that was installed with the Vagrant box (since the Spring Boot web application's JAR file includes a pre-configured, embedded web server by default - tomcat)

    - clone the application's files from the remote repository

    - build the project described on Maven pom.xml file and install the resulting artifact (JAR file) into the local Maven repository (folder 'target')

**3 |** The provisions for the `g3web` VM foreseen on the Vagrantfile were:

[Vagrantfile](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/Vagrantfile)

```
 
    config.vm.define 'g3web' do |g3web|
        g3web.vm.box = 'ubuntu/bionic64'
        g3web.vm.hostname = 'g3web'
        g3web.vm.network 'private_network', ip: '192.168.123.11'

        g3web.vm.provider 'virtualbox' do |v|
            v.memory = 2048
        end

        g3web.vm.network 'forwarded_port', guest: 8080, host: 8080
        g3web.vm.network 'forwarded_port', guest: 3000, host: 3000

        g3web.vm.provision 'shell', inline: $scriptWEB

        g3web.vm.provision "shell", run: "always", inline: <<-SHELL
            sudo nohup java -jar /home/vagrant/devops_g3_final_project/target/training-1.0-SNAPSHOT.jar &
        SHELL
    end

    $scriptWEB = <<-'SCRIPT'
        echo "-------------------- updating package lists"
        sudo apt-get update -y
        echo "-------------------- installing JDK"
        sudo apt-get install openjdk-8-jre -y
        sudo apt-get install openjdk-8-jdk -y 
        echo 'export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/' >> /etc/profile
        echo 'export PATH=$PATH:$JAVA_HOME/bin/' >> /etc/profile   
        echo "-------------------- installing Maven"
        sudo apt install maven -y
        echo 'export M2_HOME=/usr/share/maven/' >> /etc/profile
        echo 'export MAVEN_HOME=/usr/share/maven/' >> /etc/profile
        echo 'export PATH=$M2_HOME/bin:$PATH' >> /etc/profile
        echo "-------------------- installing Nodejs and NPM via Nodejs Version Manager"
        sudo apt-get update -y
        curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
        sudo apt-get install -y nodejs
        echo "-------------------- uninstalling Tomcat 8"
        sudo apt-get purge --auto-remove tomcat8
        echo "-------------------- cloning the project from repository"
        git clone https://JoCorreia@bitbucket.org/emc285/devops_g3_final_project.git
        cd devops_g3_final_project
        echo "-------------------- assembling the application"
        mvn install -DskipTests
        echo "-------------------- preparing for run frontend and backend concurrently"
        sudo chown -R $USER /home/vagrant/devops_g3_final_project/
        npm install
    SCRIPT

```

### Provisioning for the Jenkins and Ansible VM (VM hostname: 'ansible')

**1 |** The VM `ansible`was provisioned to provide support for building a pipeline using Jenkins and Ansible. It was required to provision for supporting the following steps:

  - Use a DataBase Management System, alternative to the H2 Database: PostgreSQL has been selected;

  - Assemble the application;
    
  - Generate and publish javadoc in Jenkins
    
  - Execute the application tests and publish its results in Jenkins, including code coverage; 
    
  - Publish the distributable artifacts on Jenkins (as the JAR file)

  - Use Docker for building 2 images (one for `g3web` and `g3db`) and publish it on Docker Hub

  - Use Ansible for deploying the web application and its database 

**2 |** The configuration of the VM `ansible` includes:

  - IPv4 address assigned to the VM was: 192.168.33.10;

  - The Port 8080 on the VM was forwarded to Port 8081 on the host machine, for exposing Jenkins Graphical User Interface;
    
  - the Vagrant box envimation/ubuntu-xenial was installed on the VM

  - It was required to install Ansible, Jenkins (via its WAR file), Java and Maven (for running Jenkins, which is based on Java) and Docker Engine (for building the images on the pipeline)

  - After running Jenkins via executing the command `java -jar jenkins.war`, the ping module could be used to connect `ansible` VM with the web and db VMs, using the following commands:

    ```
    $ cd /home/vagrant/vagrant
    $ ansible g3web -i hosts -m ping
    $ ansible g3db -i hosts -m ping
    ```

  - Setup the environmental variables globally for JAVA_HOME (JDK 8), MAVEN_HOME (for Maven 1) and M2_HOME (for Maven 2 and later)

  - In order to enable Ansible to connect with the other 2 VMs, it was required to create an inventory of the db and web VMs. This inventory established the path for connecting with the mentioned VMs via ssh, logging in by using the VMs primary keys. Such inventory was composed of 2 files: 'hosts' and 'ansible.cfg'. 


**3 |** The provisions for the `g3db` VM foreseen on the Vagrantfile were:

[Vagrantfile](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/Vagrantfile)

```
    config.vm.define 'ansible' do |ansible|
        ansible.vm.box = 'envimation/ubuntu-xenial'
        ansible.vm.hostname = 'ansible'
        ansible.vm.network 'private_network', ip: '192.168.33.10'

        ansible.vm.provider 'virtualbox' do |v|
            v.memory = 2048
        end

        ansible.vm.synced_folder '.', '/vagrant', mount_options: ['dmode=775,fmode=600']

        ansible.vm.network 'forwarded_port', guest: 8080, host: 8081

        ansible.vm.provision 'shell', inline: <<-SHELL
            sudo apt-get remove apache2* -y
            sudo apt-get install nano -y
            sudo apt-get install -y --no-install-recommends apt-utils
            sudo apt-get install software-properties-common --yes
            sudo apt-add-repository --yes --u ppa:ansible/ansible
            sudo apt-get install ansible --yes
            sudo apt install git-all -y
            sudo apt-get install openjdk-8-jre -y
            sudo apt-get install openjdk-8-jdk -y 
            sudo apt install maven -y
            sudo apt install docker.io -y
            sudo chmod 666 /var/run/docker.sock
            echo 'export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/' >> /etc/profile
            echo 'export PATH=$PATH:$JAVA_HOME/bin/' >> /etc/profile
            echo 'export M2_HOME=/usr/share/maven/' >> /etc/profile
            echo 'export MAVEN_HOME=/usr/share/maven/' >> /etc/profile
            echo 'export PATH=$M2_HOME/bin:$PATH' >> /etc/profile
            # For jenkins
            wget 'http://mirrors.jenkins.io/war-stable/latest/jenkins.war'
        SHELL
    end
```

### Running Vagrant

**1 |** To use Vagrant it is required to have it installed on the host computer; Vagrant is available at: https://www.vagrantup.com/downloads.html.

**2 |** On a terminal, as Powershell, move the path to the folder directory where the Vagrantfile is located (on the example below, it was provided the absolute path for the folder):

```
$ cd "C:\PATH\devops_g3_final_project"
```
**3 |** To create the 3 VMs, according to the provisions lay down on the Vagrantfile (composed of all the scripts mentioned previously), use the following command:

```
$ vagrant up
```

### Post-provisioning actions

**1 |** For all the 3 VMs created, it was required to execute further actions, that were not provisioned by the Vagrantfile.

**2 |** From the same path above mentioned, it is possible to connect to each VM via SSH, using the following commands:

```
$ vagrant ssh g3db
$ vagrant ssh g3web
$ vagrant ssh ansible
```

### Post-provisioning actions for the 'g3db' VM

**1 |** To provide a more user friendly support for managing the DBMS PostgreSQL, it was installed on the g3db VM the open-source GUI pgAdmin4, which requires Apache2.

**2 |** Log into the `g3db` VM via SSH (at the directory where the Vagrantfile lives), using:

```
$ vagrant ssh g3db
```

**3 |** Once logged into the `g3db` VM, check if the VM has all the features provisioned on the Vagrantfile:


![Chaneges on pg_hba file](Images/01_g3db_pg_hba.JPG)

![Changes on postgresql.conf file](Images/02_g3db_postgresql_conf.JPG)

![User and table created on PostgreSQL server](Images/03_g3db_table_user.JPG)


**4 |** To install the pgAdmin4 packages, use the following commands:

```
$ sudo apt install pgadmin4 pgadmin4-apache2
$ sudo apt-get upgrade -y
$ sudo apt install pgadmin4 pgadmin4-apache2` (re-run this command to ensure there are not additional features to install, from the upgrade run previously)
```
**5 |** During the installation of pgAdmin, it willbe required to define a username and a password:

 - User/email: postgres@localhost

 - Password: g3devops

**6 |** Open a browser at the IP location defined for the VM, followed by '/pgadmin4/': http://192.168.123.12/pgadmin4/.

**7 |** Insert the username/password defined on the server to log into the pgAdmin Dashboard

 - email (username): postgres@localhost

 - password: g3devops

**8 |** To add the database created on the `g3db` VM, click over 'Add Server' button, type the PostgreSQL Server details and click save:

 - Database name : testbd

 - IP address: 192.168.123.12
    
 - Port :5432

 - DB user details:

    - username: user_devops

    - password: supers3cret

![pgAdmin: welcome screen](Images/04_pgAdmin_welcomeScreen.JPG)

![pgAdmin: Database settings](Images/05_pgAdmin_setup_db.JPG)

![pgAdmin: Database query](Images/06_pgAdmin_query.bmp)

### Post-provisioning actions for the `g3web` VM

**1 |** To render the application's User Interface, log into the `g3web` VM via SSH (at the directory where the Vagrantfile lives), using:

```
$ vagrant ssh g3web
```

**2 |** Once logged into the `g3web` VM, check if the VM has all the features provisioned on the Vagrantfile:


![VM g3web: Java and Node processes runningAlt text](Images/07_g3web_java_node_processes.JPG)

**3 |** Move into the directory `cd /home/vagrant/devops_g3_final_project/` and run the command `npm start`.
  
Check at the browser that the User Interface is working as expected, on localhost:3000. Postman requests can also be made to check the application integrity (embedded tomacat default port is 8080):

![User Interface: welcome screen](Images/08_UI_WelcomeScreen.JPG)

![User Interface: Display of ledger transactions for user paulo@gmail.com](Images/09_UI_MyPage_Ledger_paulo.bmp)

![Postman: Get request of all ledger transactions for user paulo@gmail.com.](Images/10_Postman_MyPage_Ledger_paulo.JPG)

To add the all changes and files to the remote repository and merge the branch 'playbook' with branch 'master' we execute:

```
$ git add --all
$ git commit -a -m "close #5 – Creation and development of a playbook.yml file"
$ git push -u origin playbook
$ git checkout master
$ git merge playbook
$ git push
```

## 4 | Using Maven to Build the Project


**Maven**, or Apache Maven, is a **build automation tool** used for Java projects.
  
Each project contains a file called POM (Project Object Model), which is just an XML file containing details of the project.
  
Some of these details might include project name, version, package type, dependencies, Maven plugins, etc.
  
Every time that jenkins build the assemble stage, Maven will be called to compile the source code from the project.

Maven will be installed in the same VM of Jenkins when the following command will be executed:

```
$ sudo apt install maven -y
```

Related with the maven installation the following environment variables will be defined:

```
echo 'export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/' >> /etc/profile
echo 'export PATH=$PATH:$JAVA_HOME/bin/' >> /etc/profile
echo 'export M2_HOME=/usr/share/maven/' >> /etc/profile
echo 'export MAVEN_HOME=/usr/share/maven/' >> /etc/profile
echo 'export PATH=$M2_HOME/bin:$PATH' >> /etc/profile
```

This environment variables are loaded whenever a bash login shell is entered, for this they are added in the `/etc/profile`.
  
  
## 5 | Using Ansible to Deploy and Configure the Application and its Database


**Ansible** is a simple **IT automation engine** that **automates cloud provisioning**, **configuration management**, **application deployment**, **intra-service orchestration**, and many others IT needs.

Designed for multi-tier deployments, Ansible models the IT infrastructure by describing how all of the systems inter-relate, rather than just managing one system at a time.

It does not use agents or additional custom security infrastructure, so it's easy to deploy.
Added to this, it uses a very simple language (YAML, in the form of Ansible Playbooks) that allow you to describe your automation jobs in a way that approaches plain English.

The main goal of Ansible in our project was to update the .jar file initialy executed by the Vagrantfile to a new .jar file generated during the build of the Jenkins pipeline.

So, it will be necessary to create in Jenkinsfile a connection point to the playbook of Ansible, which can be translated into a stage.

### Create a new branch 'playbook'

To create and add the Ansible Playbook to the Personal Finance Project we create an independent line of development (branch), executing:
  
```
$ git checkout -B playbook
```
 
### Ansible Playbook

[Playbook](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/playbook1.yml)
 
```
 - hosts: g3web
  become: yes
  tasks:
    - name: Kill previous backend build
      command: pkill -f "java -jar /home/vagrant/devops-mvn-pgsql/target/training-1.0-SNAPSHOT.jar"
      ignore_errors: yes
    - name: Update project
      shell: cd /home/vagrant/devops-mvn-pgsql ; git pull https://JoCorreia@bitbucket.org/JoCorreia/devops-mvn-pgsql.git
    - name: Copy
      copy:
        src:  "{{ lookup('env','WORKSPACE') }}/target/training-1.0-SNAPSHOT.jar"
        dest: "/home/vagrant/devops-mvn-pgsql/target"
    - name: Run jar
      shell: nohup java -jar /home/vagrant/devops-mvn-pgsql/target/training-1.0-SNAPSHOT.jar &
```

The execution of the *playbook1* will trigger the following actions:

 - The ansible VM, which contains Ansible installed, enters, by ssh session, in the g3web VM, which is responsible for the continuous integration part of our application;

 - Stop the execution of the previous .jar file, where we define a step that ignores errors that appear if the .jar file has not yet been created, which allows the playbook to continue running;

 - Update the project making a pull to the repository, in order to integrate the changes that occurred when Vagrantfile is executed;

 - Copy the .jar file from the root folder of the Jenkins build "workspace" and the remaining path, already fixed, composed of the target folder and the file name, to the destination folder indicated on the code above;

 - Execute the .jar file, using the nohup tool to keep the execution of it occuring in the background, in a way that allows Jenkins to finalize the pipeline build.

  
To add the all changes and files to the remote repository and merge the branch 'playbook' with the branch 'master' we execute:

```
$ git add --all
$ git commit -a -m "close #5 – Creation and development of a playbook.yml file"
$ git push -u origin playbook
$ git checkout master
$ git merge playbook
$ git push
``` 
  
## 6 | Using Docker to create Docker Images
  
**Docker** is a tool designed to make it easier to **create, deploy and run applications** by using **Linux containers**.

Docker containers and virtual machines (VMs) are very similar, their main differences are in terms of scale and portability.

Compared to VMs, containers are best when we need to:
  
  - Maximize the number of apps running on a server.
  - Deploy multiple instances of a single application.
  - Have a lightweight system that quickly starts.
  - Develop an application that runs on any underlying infrastructure.
  
Compared to containers, VMs are best when we need to:
  
  - Manage a variety of operating systems.
  - Manage multiple apps on a single server.
  - Run an app that requires all the resources and functionaries of an Operative System.
  - Ensure full isolation and security.
  
### Create a new branch 'dockerimages'

To working with Docker Engine into the Personal Finance Project we create an independent line of development (branch), executing:
  
```
$ git checkout -B dockerimages
```

### Working with Docker Engine

First of all, and in order to check if Docker was installed on the machine, we need to run command ``docker -v``, having the following outcome:

![](./Images/DOCKER_VERSION.PNG)

In order to build the images with the correct information it is necessary to create the **WEB Dockerfile** having in consideration the following points:

 * Add the repository from where the image will clone the project;
 * Change the ``WORKDIR`` where the application will be located;
 * Run the command ``mvn clean install -DskipTests`` to build the project of which we want to create a Docker image;
 * Install the node package manager executing the command ``npm install``;
 * Define the name of the Java archive file to ``training-1.0-SNAPSHOT.jar``;
 * Expose the docker image on the port 3000.
 
[Web Dockerfile](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/web/Dockerfile)
 
```

    FROM tomcat
    
    RUN apt-get update -y
    
    RUN apt-get install -f
    
    RUN apt-get install git -y
    
    RUN apt-get install nodejs -y
    
    RUN apt-get install npm -y
    
    RUN apt-get install nano -y
    RUN apt install maven -y
    
    RUN mkdir -p /target/
    
    WORKDIR /target/
    
    RUN git clone https://bitbucket.org/emc285/devops_g3_final_project.git
    
    RUN chmod -R 777 /target/devops_g3_final_project
    
    WORKDIR /target/devops_g3_final_project
    #WORKDIR /target
    
    #RUN mvn spring-boot:run -e
    RUN mvn clean install -DskipTests
    
    
    RUN rm -rf node_modules
    
    RUN npm i npm@latest -g
    RUN npm cache clean --force
    RUN npm install
    
    WORKDIR /target/devops_g3_final_project
    
    # CMD ["npm", "start"]
    RUN mkdir -p /scripts
    COPY startcommands.sh /scripts/startcommands.sh
    RUN ["chmod", "+x", "/scripts/startcommands.sh"]
    WORKDIR /scripts
    RUN sed -i -e 's/\r$//' startcommands.sh
    ENTRYPOINT ["/scripts/startcommands.sh"]
    
    
    EXPOSE 3000
    EXPOSE 8080

```

It was also decided that we would create and add a simple script ``startcommands.sh`` that will run the ``jar`` file and the command ``npm start``,
in order to start both the Spring Boot app and React app.

[startcommands.sh](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/web/startcommands.sh)

```

    #!/bin/bash
    # npm start &
    # $CATALINA_HOME/bin/catalina.sh run &
    
    
    java -jar /target/devops_g3_final_project/target/training-1.0-SNAPSHOT.jar &
    
    cd /target/devops_g3_final_project
    npm start
    tail -f /dev/null

```

 Regarding the creation of the database container, using a **DB Dockerfile**, we need to considerate the following points:

 * Add PostgreSQL's repository, from where it is possible to download the most recent stable release;
 * Install PostgreSQL 9.3;
 * Adjust PostgreSQL configuration in order to enable the remote connections to the database;
 * Enable remote connections to the PostgreSQL server from outside;
 * Expose the docker image on the port 5432 (PostgreSQL port).

[DB Dockerfile](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/db/Dockerfile)

```
 FROM ubuntu:16.04
 
 RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8
 RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list
 
 RUN apt-get update && apt-get install -y python-software-properties software-properties-common postgresql-9.3 postgresql-client-9.3 postgresql-contrib-9.3
 
 USER postgres
 
 RUN    /etc/init.d/postgresql start &&\
     psql --command "ALTER USER postgres PASSWORD 'postgres';"

 RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.3/main/pg_hba.conf
  
 RUN echo "listen_addresses='*'" >> /etc/postgresql/9.3/main/postgresql.conf
 
 EXPOSE 5432
 
 VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]
 
 CMD ["/usr/lib/postgresql/9.3/bin/postgres", "-D", "/var/lib/postgresql/9.3/main", "-c", "config_file=/etc/postgresql/9.3/main/postgresql.conf"]
```

To add the all changes and files to the remote repository and merge the branch 'dockerimages' with branch 'master' we execute:

```
$ git add --all
$ git commit -a -m "close #6 – Development of the dockerfiles for the creation of web and database images"
$ git push -u origin dockerimages
$ git checkout master
$ git merge dockerimages
$ git push
``` 
  
## 7 | Using Jenkins to Build Stages of a Pipeline

**Jenkins** is a free and open source **automation tool** to **Continuous Integration** (CI) and **Continuous Delivery** (CD) process.
  
It works like a server capable of orchestrating a chain of actions that help developers on the CI/CD workflows, called by pipelines.
  
With its wide range of plugins, Jenkins supports the build, the deploy, and the automation of any project.
    
This automation makes it easier to integrate project changes, and easier to obtain a fresh build, which increases the developers productivity.

### Create a new branch 'jenkinsfile'
  
To create a jenkinsfile we should create an independent line of development (branch).
  
To create new branch and starting to work on it we must execute:
  
```
$ git checkout -B jenkinsfile
```
  
### Up the machines

Already on the repository folder, we must up the machines (db, web and ansible) throw the vagrantfile.
Using the command line we execute:

```
$ vagrant up
```

### Run Jenkins

Toward to run Jenkins it is necessary,  in a first moment, accede to the  virtual machine where it is installed: the **Ansible VM**.
To access the VM by **shh session** we must execute:

```
$ vagrant ssh ansbile
```

Already on Ansible VM we **run the jar command**, executing:

```
$ java -jar jenkins.war
```
  
 Note that Jenkins should still running during all the next processes.
    
### Accede Jenkins

Now we must access to the **localhost** through the **IP of the ansible VM**: http://192.168.33.10:8080
	
  - To **unlock Jenkins** use the password automatically generated on *PowerShell*
	  	
  - Already on **Jenkins page** we should:
	
	- 1st: **Customize Jenkins**: Install suggested plugins.
	- 2nd: **Create first admin user**: Define Username; Password; Full Name and Email.
	- 3rd: **Instance configuration**: Define Jenkins URL.
	 
  
### Create a new Job

The Job concept is applied to a new process/project that will be performed.
  
Each job consists of stages that are, in the end, logical groupings of steps.
  
That steps correspond to new tasks that are performed on one or more Nodes.  
In the absence of any indication (additional settings) the Node assumed by Jenkins is the computer where Jenkins is running.

To create a new job, at **Jenkins page**, we must:

  - Select **New Item**
  - Define an **item name**: *devopsG3Main*
  - Choose the **Pipeline** option
  
Now we can define some settings like add a Description or define Build Triggers, but we will just focus on the Pipeline options.
    
Already on the **Pipeline guide**, we must:
  
  - Select the **Pipeline Definition** as *Pipeline script from SCM*
  
	If the intention were to write the text of the pipeline, we would choose the option Pipeline Script.
	As we intend to use a Jenkinsfile where we will define all the Job Stages, we must choose the Script from SCM.
	  
	Note that: The Jenkinsfile must have on the root of the project.
  
  - Select the **SCM** as *Git*
    As we intent to use GIT to access at the repository.
    
  - Define the **Repository URL**: https://1050757@bitbucket.org/emc285/devops_g3_final_project
   Note that any credentials will be required, since this repository is public, otherwise they would have to be defined.
   
  - Define the **Script Path** to the directory of Jenkinsfile: devops/Jenkinsfile


#### Create a Jenkinsfile

To create a Jenkinsfile, on the root directory of project, we execute:

```
$ echo JENKINSFILE >> Jenkinsfile
```
  
**CREATE THE STAGES OF PIPELINE**

  * CHECKOUT - Check out the code from the repository.
  * ASSEMBLE - Compiles the source code from the project.
  * TESTS - Compiles tests.
  * JAVADOC - Generates the javadoc of the project and publish it in Jenkins.
  * ARCHIVING - Archives in Jenkins the tests (generated during Tests Stage, in the index.html).
  * BUILD - Compile, converts and archives the project into a Java program (.jar).
  * DEPLOY - Deploy the configurations selected in playbook on the established hosts.
  * DOCKER IMAGE WEB: Generate a docker image for web and publish it in the Docker Hub.
  * DOCKER IMAGE DB: Generate a docker image for db and publish it in the Docker Hub. 
  
#### Add Jenkinsfile to the repository

[Jenkinsfile](https://bitbucket.org/emc285/devops_g3_final_project/src/master/devops/Jenkinsfile)
  
``` 
  pipeline {

  environment {
    registry = "devops1050757/devopsmain"
  }
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/emc285/devops_g3_final_project'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assemble...'
                sh 'mvn clean compile'
            }
        }
        stage('Tests') {
            steps {
                 sh 'mvn test'
            }
        }
		stage('Javadoc') {
			steps {
			    dir(path: '.') {
				    echo 'Generating the Javadoc documentation...'
				    sh 'mvn javadoc:javadoc'
			    }
            }
        }
        stage('Archiving') {
            steps {
                dir(path: '.') {
                    publishHTML([allowMissing: true, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/site/jacoco/', reportFiles: 'index.html', reportName: 'Code Coverage', reportTitles: 'Code Coverage'])
                    step([$class: 'JUnitResultArchiver', testResults: 'target/surefire-reports/TEST-*.xml'])
                }
            }
        }
        stage('Build'){
            steps {
                sh 'mvn install -DskipTests'
            }

        }
        stage ('Publish Docker Image web') {
            steps {
            dir("devops/web"){
            script {
            docker.withRegistry('https://registry.hub.docker.com', 'DockerHubCredentials') {
            def test = docker.build registry + "web:$BUILD_NUMBER"
            test.push()
                         }
                    }
                }
            }
        }
        stage ('Publish Docker Image db') {
            steps {
            dir("devops/db"){
            script {
            docker.withRegistry('https://registry.hub.docker.com', 'DockerHubCredentials') {
            def test = docker.build registry + "db:$BUILD_NUMBER"
            test.push()
                          }
                     }
                }
            }
        }
        stage('Deploy') {
            steps {
                    echo 'Deploying the application ...'
                    ansiblePlaybook(
                    inventory: '/vagrant/hosts',
                    playbook: '/vagrant/playbook1.yml'
                )
            }
        }
	}/* Stages*/

  }  /* pipeline */

``` 

To update all the changes and files to remote repository and merge the branch 'jenkinsfile' with the branch 'master' we execute:
 
```
$ git add --all
$ git commit -m "close #7 – Create Jenkinsfile pipeline"
$ git push -u origin jenkinsfile
$ git checkout master
$ git merge jenkinsfile
$ git push
```

#### ADD JENKINS PLUGINS

The Jenkinsfile is already on the remote repository but before build it we need to add the following plugins:

   1. Docker Pipeline
    
   2. Docker-build-step
    
   3. Html publisher
    
   4. Jacoco
  
  
**BUILD THE PIPELINE**
  
On Jenkins page we build the pipeline and after that we can see that all stages have ran correctly:

![](./Images/build.jpg)
  
![](./Images/testResults.jpg)
  
![](./Images/jacocoCoverage.jpg)

![](./Images/javadoc.jpg)
  
![](./Images/workspace.jpg)
  
  
#### Add tags to the work development

When each of the features are accomplished, we put the respective tag and push that to the remote repository:    

```  
$ git tag alt1.0.0 2df1b8f
$ git tag main1.0.0 5633241
$ git tag main1.1.0 db3ea4d
$ git tag main1.2.0 06236df
$ git tag main1.3.0 39a48ac
$ git tag main1.4.0 b0105be
$ git tag main1.5.0 c966b49
$ git tag main1.6.0 a35f5dc
$ git tag main1.2.1 65f1db3
$ git tag main1.4.1 bdfe5e4
$ git tag main1.6.1 dc447ae
$ git tag projectfinal
$ git push --tags
```  
  
# IMPLEMENTATION OF AN ALTERNATIVE 
  

To implement an alternative we need to start working on the Personal Finance Project **fork** previously created: **devops_g3_alternative**.

Executing the following command line, we change the work directory to the folder of fork:

```
$ cd "C:\Users\USER\Desktop\devops_g3_alternative"
```  
  
## 1 | Bitbucket and Git 
  
  
Already working on the alternative fork, the first step must be parcelling the problem or challenge, as represent the following issues:

### Alternative Main Tasks / Issues

  * First Commit - Create 'Devops' Folder
  * Create a Gradle build project
  * Update the db for postgreSQL
  * Create Docker Images
  * Create a Jenkinsfile pipeline 
  * Create Docker Container for Jenkins
  * Create a Docker-Compose
  
### Prepare the work directory

With all parts of the problem defined we should create a main directory 'devops' and start to working on it, executing:

```
$ mkdir devops
$ cd "C:\Users\USER\Desktop\devops_g3_alternative\devops"
```

Inside of 'devops' directory, should also be created others directories that will be needed, namely: web, database and jenkins. To create this directories and a simple file to commit with that folders we execute:

```
$ mkdir web
$ mkdir db
$ mkdir jenkins
$ echo files>>allFilesHere
```

All the directories have been created and now they should be added to the remote repository executing:

```
$ git commit -m " fix#1 First Commit - Create 'Devops' Folder"
$ git push
```

## 2 | Using Gradle to Build the Project

**Gradle** is an **open source build automation system** that uses the concepts of Apache Maven and Apache Ant.
  
It uses domain-specific language based on the programming language Groovy, differentiating it from Apache Maven, which uses XML for its project configuration.
  
Gradle also determines the order of tasks run by using a directed acyclic graph.

As an alternative to Maven, Gradle will be the tool used to build the project.

### Create a new branch 'gradlebuild'
  
To update the Personal Finance Project to Gradle we should create an independent line of development (branch).
  
To create new branch and starting to work on it we must execute:
  
```
$ git checkout -B gradlebuild
```

With the 'gradlebuild' branch created we delete all the files of the Personal Finance Project with the exception of **.idea** and **src** folders, and **.gitignore** file.

To save the changes in remote repository we execute the following command line:

```
$ git commit -a -m "ref#2 change the project from maven to gradle as a build tool"
$ git push -u origin gradlebuild 
```

### Start a Gradle Spring Project

To start a new gradle spring project we can use the **spring initializr** at https://start.spring.io.
  
Already on spring initializr, we select the option **gradle project** and add the respective **dependencies** that will be needed to our project such as: Spring Data JPA; H2 Database, Spring Web, Rest Repositories, Thymeleaf.

![](./Images/4_DevopsAltGradle.png)
 
As result of this step we will get a **zip file** with a **spring boot application** that we can build with Gradle.
 
### Add Gradle Modules to the Personal Finance Project

Unzipping the zip file obtained at spring initializr, we verify that it is composed by the files:

  - build.gradle
  - gradle/wrapper/gradle-wrapper.jar
  - gradle/wrapper/gradle-wrapper.properties
  - gradlew
  - gradlew.bat
  - settings.gradle

A src folder also compose this zip file but, not being in our interest, it will only be necessary to, using the windows environment, **copy the gradle files** to the folder of the alternative fork.  

The build.gradle has already the necessary pluggins and dependencies needed for the project to work:  

 - Plugins
  
```
    plugins {
    id 'org.springframework.boot' version '2.3.1.RELEASE'
    id 'io.spring.dependency-management' version '1.0.9.RELEASE'
    id 'java'
    }
```
    

 - Maven Repositories
     
```   
    repositories {
    mavenCentral()
    maven { url 'https://repo.spring.io/milestone' }
    maven { url 'https://repo.spring.io/snapshot' }
    }
```  
     
 - Necessary dependencies to start the project:  
     
```   
    dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-data-rest'
    implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    runtimeOnly 'com.h2database:h2'
    testImplementation('org.springframework.boot:spring-boot-starter-test') {
    exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }
    }
```

 - Add the task to execute JUnit tests
     
```   
    test {
    useJUnitPlatform()
    }
```  


To update the changes in remote repository we must execute:

```
$ git add --all
$ git commit -a -m "ref #2 change gitignore and add some gradle modules"
$ git push -u origin gradlebuild
```
  
### Update *.gitignore* file
  
Back in the alternative fork of Personal Finance project, and to prevent that unnecessary files being sent to the remote repository, the following dependencies must be added to the *.gitignore* file:

[.gitignore](https://bitbucket.org/emc285/devops_g3_alternative/src/master/.gitignore)

```
  #dependencies
  /node_modules
  /.pnp
  .pnp.js
```

To update the changes in the remote repository we execute:

```
$ git commit -a -m "ref #2 change gitignore to ignore node and node.js"
$ git push -u origin gradlebuild
``` 
  
### Update project using the SRC developed

With the Personal Finance Project already with Gradle, the last version of the **source folder** ('src') of the main project can be added to the alternative fork.
  
Using the windows environment, we should **copy the gradle files** to the folder of the alternative fork.

To update the changes in remote repository we execute:

```
$ git add --all
$ git commit -a -m "ref#2 update the project"
$ git push -u origin gradlebuild
```

### Update the *build.gradle* file to jacoco and javadoc


[build.gradle](https://bitbucket.org/emc285/devops_g3_alternative/src/master/build.gradle)

Update plugins and add Jacoco and Javadoc
    
```   
    apply plugin: 'jacoco'
    apply plugin: 'java'
       
    javadoc {
    source = sourceSets.main.allJava
    failOnError = false
    }
```  
    
To update all the changes and files to remote repository and merge the branch 'gradlebuild' with the branch 'master' we execute :

```
$ git add --all
$ git commit -a -m "ref#2 update the build.gradle to jacoco and javadoc"
$ git push -u origin gradlebuild
$ git checkout master
$ git merge gradlebuild 
$ git push
```

## 3 | PostgreSQL database as alternative to H2 database

### Create a new branch 'postgreSQL'
  
To update the H2 database to PostgreSQL database we should create an independent line of development (branch).
  
To create new branch and starting to work on it we must execute:
  
```
$ git checkout -B postgreSQL
```

### Add PostgreSQL dependency
	
Add the postgreSQL dependency to build.gradle file:

[build.gradle](https://bitbucket.org/emc285/devops_g3_alternative/src/master/build.gradle)

```
 runtime 'org.postgresql:postgresql:9.4-1206-jdbc42'
```

To update the changes in remote repository we execute:


```
$ git add --all
$ git commit -a -m "ref #3: add the postgree dependency to build.gradle"
$ git push -u origin postgreSQL
```

### Change the applications properties to the new db definitions

We need to update the *application.properties* file based on the Spring framework configuration properties for the adoption of a PostgreSQL database.
 
[application.properties](https://bitbucket.org/emc285/devops_g3_alternative/src/master/src/main/resources/application.properties)

```
	spring.main.banner-mode=off
	logging.level.org.springframework=ERROR

	spring.jpa.hibernate.ddl-auto=create-drop

	spring.datasource.initialization-mode=always
	spring.jpa.database=POSTGRESQL
	spring.datasource.platform=postgres

	spring.datasource.url=jdbc:postgresql://database:5432/postgres
	spring.datasource.username=postgres
	spring.datasource.password=postgres

	spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true

```

To update all the changes and files to remote repository and merge the branch 'postgreSQL' with the branch 'master' we execute :

```
$ git add --all
$ git commit -a -m "ref #3: change the applications properties to the new db definitions"
$ git push -u origin postgreSQL
$ git checkout master
$ git merge postgreSQL
$ git push
```

## 4 | Using Docker to create Docker Images

### Create a new branch 'dockerimages'
  
To create the docker images of we and db we should create an independent line of development (branch).
  
To create a new branch and start to work on it, we must execute:
  
```
$ git checkout -B dockerimages
```
  
### Create a dockerfile to WEB

To create the respective dockerfile, inside the 'devops/web' directory, we execute:

```
$ echo DOCKERFILE >> Dockerfile
```

It was also decided that we would create and add a simple script ``startcommands.sh`` that will run the ``jar`` file generated during the build and the command ``npm start``,
in order to start both the Spring Boot app and React app.

```
    #!/bin/bash
    
    
    java -jar /tmp/build/devops_g3_alternative/build/libs/project-0.0.1-SNAPSHOT.jar &
    
    cd /tmp/build/devops_g3_alternative
    npm start 
    tail -f /dev/null

```

[Web Dockerfile](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/web/Dockerfile)

```

 FROM tomcat
 
 RUN apt-get update -y
 
 RUN apt-get install -f
 
 RUN apt-get install git -y
 
 RUN apt-get install nodejs -y
 
 RUN apt-get install npm -y
 
 
 RUN mkdir -p /tmp/build
 
 WORKDIR /tmp/build/
 
 RUN git clone https://emc285@bitbucket.org/emc285/devops_g3_alternative.git
 
 RUN chmod -R 777 /tmp/build/devops_g3_alternative
 
 WORKDIR /tmp/build/devops_g3_alternative
 
 RUN ./gradlew clean build -x test
 
 RUN rm -rf node_modules
 
 RUN npm i npm@latest -g
 RUN npm cache clean --force
 RUN npm install
 

 WORKDIR /tmp/build/devops_g3_exp 
 
 RUN mkdir -p /scripts       
 COPY startcommands.sh /scripts/startcommands.sh        
 RUN ["chmod", "+x", "/scripts/startcommands.sh"]
 WORKDIR /scripts
 RUN sed -i -e 's/\r$//' startcommands.sh
 ENTRYPOINT ["/scripts/startcommands.sh"]
 
 
 EXPOSE 3000
 EXPOSE 8080

```

### Create a dockerfile to Database

To create the respective dockerfile, inside the 'devops/db' directory, we execute:

```
$ echo DOCKERFILE >> Dockerfile
```

[DB Dockerfile](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/db/Dockerfile)

```

 FROM ubuntu:16.04
 
 RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8
 
 RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list
 
 RUN apt-get update && apt-get install -y python-software-properties software-properties-common postgresql-9.3 postgresql-client-9.3 postgresql-contrib-9.3
 
  USER postgres
 
 RUN    /etc/init.d/postgresql start &&\
     psql --command "ALTER USER postgres PASSWORD 'postgres';"
 
 RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.3/main/pg_hba.conf
 
 RUN echo "listen_addresses='*'" >> /etc/postgresql/9.3/main/postgresql.conf
 
 EXPOSE 5432
 
 VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]
 
 CMD ["/usr/lib/postgresql/9.3/bin/postgres", "-D", "/var/lib/postgresql/9.3/main", "-c", "config_file=/etc/postgresql/9.3/main/postgresql.conf"]

```

To update all the changes and files to remote repository and merge the branch 'dockerimages' with the branch 'master' we execute :

```
$ git add .
$ git checkout -- ../.DS_Store
$ git commit -m "ref #4 Create Docker Images for web and db"
$ git push -u origin dockerimages
$ git checkout master
$ git merge dockerimages
$ git push 
```

## 5 | Using Jenkins to Build Stages of a Pipeline

### Create a new branch 'jenkinsfile'
  
To create a jenkinsfile we should create an independent line of development (branch).
  
To create new branch and starting to work on it we must execute:
  
```
$ git checkout -B jenkinsfile
```

### Create a Jenkinsfile

To create a Jenkinsfile, on the root directory of project ('devops' folder), we execute:

```
$ echo JENKINSFILE >> Jenkinsfile
```
  
**CREATE THE STAGES OF PIPELINE**

To configure the pipeline in Jenkins where CI is performed, it is necessary to create the Jenkinsfile with the necessary stages.

The stages that must compose the Jenkinsfile are:

  * CHECKOUT - To check out the code from the repository.
  * ASSEMBLE - To compiles the source code from the project;
  * TEST - Executes the Tests and publish in Jenkins the Test results and coverage.
  * JAVADOC - Generates the javadoc of the project and publish it in Jenkins.
  * ARCHIVING - Archives the generated application artifacts, in this case a jar file;
  * PUBLISH THE DOCKER IMAGE WEB: Generate a docker image for web and publish it in the Docker Hub;
  * PUBLISH THE DOCKER IMAGE DB: Generate a docker image for db and publish it in the Docker Hub;  
  
### Add Jenkinsfile to the repository

[jenkinsfile](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/jenkinsfile)

``` 
  pipeline {
    environment {
        registry = "beta85/expdevops"
        registryCredential = 'Dockerhub'
    }
        agent any
    
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git url: 'https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/'
                }
            }
    
            stage('Assemble') {
                steps {
                    echo 'Assembling...'
                    sh './gradlew clean build --rerun-tasks -x test'
                   }
                 }
            stage('Test') {
                 steps {
                    echo 'Testing...'
                    sh './gradlew test'
                    sh './gradlew jacocoTestReport'
                    junit '**/test-results/test/*.xml'
                    step( [ $class: 'JacocoPublisher' ] )
                     }
                  }
            stage('Javadoc') {
                  steps {
                        echo 'Generating Javadoc documentation...'
                        sh './gradlew javadoc'
    
                             }
                          }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'build/libs/*'
                }
            }
             stage ('Docker Image web') {
                 steps {
                     dir("devops/web"){
                     script {
                     docker.withRegistry( '', registryCredential ){
                     def test = docker.build registry  + "web:$BUILD_NUMBER"
                     test.push()
                     def latest = docker.build (registry + "web:latest", " --no-cache . ")
                     latest.push()
                                 }
                            }
                        }
                    }
                 }
             stage ('Docker Image db') {
                  steps {
                      dir("devops/db"){
                      script {
                      docker.withRegistry( '', registryCredential ){
                      def test = docker.build registry  + "db:$BUILD_NUMBER"
                      test.push()
                      def latest = docker.build (registry + "db:latest", " --no-cache . ")
                      latest.push()
                                  }
                              }
                          }
                  }
             }
       } 
 }
``` 

Already with the Jenkinsfile created, and before add it to remote repository, we must change GIT repository permissions executing:

```
$ git update-index --chmod=+x gradlew
```

To update all the changes and files to remote repository and merge the branch 'jenkinsfile' with the branch 'master' we execute :

```
$ git add --all
$ git commit -a -m "ref #4: create a jenkinsfile with the stages necessary to CI"
$ git push -u origin jenkinsfile
$ git checkout master
$ git merge jenkinsfile
$ git push
```
  
## 6 | Create Docker Container for Jenkins

### Create a new branch 'dockerJenkins'
  
To create a docker container we should create an independent line of development (branch).
  
To create a new branch and start to work on it, we must execute:
  
```
$ git checkout -B dockerJenkins
```

To configure the container where Jenkins will be executed, the following steps were performed:

- Create a file with the necessary plugins
- Create a jenkins.yam
- Create a jenkins Dockerfile 
- Create a Docker-compose for jenkins up
		

[plugins.txt](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/jenkins/plugins.txt)
       
```
    configuration-as-code:latest
    workflow-aggregator:latest
    blueocean:latest
    pipeline-maven:latest
    job-dsl:latest
    javadoc:latest
    jacoco:latest
    junit:latest
    gradle:latest   
``` 

[jenkins.yaml](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/jenkins/jenkins.yaml)
 
```
 jenkins:
  systemMessage: "Switch G3 \n\n"
     tool:
      git:
        installations:
        - home: "git"
          name: "Default"
      maven:
        installations:
        - name: "Maven 3"
          properties:
          - installSource:
              installers:
                - maven:
                    id: "3.5.4"
      jobs:
      - script: >
          pipelineJob('switchg3alternativa') {
              definition {
                  cpsScm {
                      scriptPath 'devops/jenkinsfile'
                      scm {
                        git {
                            remote { url 'https://emc285@bitbucket.org/emc285/devops_g3_alternative.git' }
                            extensions {}
                        }
                      }
                  }
              }
          }
``` 

[Jenkins Dockerfile](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/jenkins/dockerfile)
 
```
    FROM getintodevops/jenkins-withdocker:lts
    COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
    COPY jenkins.yaml /usr/share/jenkins/ref/jenkins.yaml
    RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
``` 

[docker-compose.yml](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/jenkins/docker-compose.yml)
 
```
    version: '3.1'
    
    volumes:
      jenkins_home:
    
    services:
      jenkins:
        build: .
        ports:
          - 9003:8080
        volumes:
        #  - /var/run/docker.sock:/var/run/docker.sock # Working on linux
          - jenkins_home:/var/jenkins_home:rw
        #  - /usr/local/bin/docker:/usr/bin/docker
          - /var/run/docker.sock:/var/run/docker.sock # Working on Windows Docker Desktop
      database:
        image: postgres
        restart: always
        environment:
          POSTGRES_PASSWORD: postgres
        ports:
          - 5432:5432
      adminer:
        image: adminer
        restart: always
        ports:
          - 8085:8080
``` 

To add the changes to the remote repository we execute:

```
$ git commit -a -m "ref #6 create a docker container for jenkins"
$ git push
$ git push -u origin dockerJenkins
```

Now we can navigate to the folder jenkins and run the docker-compose file to up the Jenkins on the port 9003 run the job in jenkins: 
  
```
$ cd "C:\Users\USER\Desktop\devops_g3_alternative\devops\jenkins"
$ docker-compose up
```  
  
  ![](./Images/12_DevopsAltJenkinsPipeline.png)  
    
    
   - Here is possible to see thes test results and de coverage:  
   
   
   ![](./Images/13_DevopsAltTestResult.png) 
  
   ![](./Images/16_DevopsAltHTML_results.png) 
  
   ![](./Images/17_DevopsAltCoverageJacoco.png)  
  
    
   - The files generated by Javadoc:  
  
   ![](./Images/18_DevopsAltJavadocIndexpng.png)
  

   - The archiving jar file generated during the build:  
     
   ![](./Images/15_DevopsAltIssueArchiveJar.png)   
     
    
    
   - And its possible to confirm that both images are pushed to docker.hub  
          
   ![](./Images/9_DevopsAltDockerHubImages.png)
     

With all the changes made regarding the creation of a docker container, we can now add the work developed to the master branch executing:

```
$ git checkout master
$ git merge dockerJenkins
$ git push
```

## 7 | Using Docker-Compose to Containerize and Deploy the parts of solution

#### Create a new branch 'dockercompose'
  
To create a dockercompose we should create an independent line of development (branch).
  
To create new branch and starting to work on it we must execute:
  
```
$ git checkout -B dockercompose
```

To set up the db and web machines, we need to create a docker-compose file, inside **devops** directory, at the same level as the **db** and **web** directories.

We need to execute:

```
$ echo >> docker-compose.yml
```

[docker-compose.yml](https://bitbucket.org/emc285/devops_g3_alternative/src/master/devops/docker-compose.yml)
 
```
    version: '2.0'
    
    services:
      web:
        image: beta85/expdevopsweb
        stdin_open: true
        ports:
          - "3000:3000"
          - "8080:8080"
      database:
        image: beta85/expdevopsdb:latest
        restart: always
        environment:
          POSTGRES_PASSWORD: postgres
        ports:
          - 5432:5432
      adminer:
        image: adminer
        restart: always
        ports:
          - 8082:8080
    
``` 

To update all the changes and files to remote repository and merge the branch 'dockercompose' with the branch 'master' we execute :

```
$ git checkout -b dockercompose
$ git add .
$ git checkout -- ../.DS_Store
$ git commit -m "ref #7 Create Docker-Compose"
$ git push -u origin dockercompose
$ git checkout master
$ git merge dockercompose
$ git push 
```  
  
When each of the features are accomplished, we put the respective tag and push that to the remote repository:    

```  
$ git tag alt1.0.0 2df1b8f
$ git tag alt1.1.0 0d4be3c
$ git tag alt1.2.0 55a6a0e
$ git tag alt1.3.0 c90ff93
$ git tag alt1.3.1 38993ae
$ git tag alt1.4.0 ad9f622
$ git tag alt1.5.0 1b887d3
$ git tag altfinal 631f8c5
$ git push --tags
```  
    
For last we can navigate to the folder 'devops' and run the docker-compose file to deploy the app and db: 
  
```
$ cd "C:\Users\USER\Desktop\devops_g3_alternative\devops"
$ docker-compose up
```  

In order to check if both images, web container and DB container, are up and running, we need to check it through the 
following URLs:

  - web - http://localhost:3000

When the application url is accessed, the following login page is displayed:

![](./Images/23_DevopsAltWeb.png)

To access the db, we first have a graphic interface, where we need to select the type of database, and the user credentials

  - db/adminer -  http://localhost:8082
 
When the database url is accessed, the following login page is displayed:
 
![](./Images/20_DevopsAlt_dbAdminer.png)

After making a successful login, it presents us with the content of our database:

![](./Images/22_DevopsAltDB.png)

If we make some changes either on db or web, those changes should be reflected on each other. 
As we can see, if we add a category on our web application, the db will be updated, for example:    
 
![](./Images/25_DevopsAltInsertCat.png)  
  
![](./Images/26_DevopsAltConfirmInDB.png)

#### Tasks Distribution
  
 All team members contributed strongly to the final result, most of the tasks were developed with the contribution of the whole group although some elements were more focused on particular tasks.
   
 Bearing this in mind, we can describe the participation of the elements from the following table:
   
![](./Images/Tasks.PNG)